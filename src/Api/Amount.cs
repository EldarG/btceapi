﻿using System;
using System.Globalization;

namespace Api
{
    public struct Amount : IEquatable<Amount>
    {
        private readonly decimal _value;

        public Amount(decimal value)
        {
            if (value <= 0)
                throw new ArgumentException("Value must be positive.", nameof(value));
            _value = value;
        }

        public bool Equals(Amount other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Amount && Equals((Amount)obj);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static bool operator ==(Amount left, Amount right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Amount left, Amount right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return _value.ToString(CultureInfo.InvariantCulture);
        }
    }
}
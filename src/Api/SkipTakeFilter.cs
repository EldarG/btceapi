﻿namespace Api
{
    public sealed class SkipTakeFilter
    {
        public SkipTakeFilter(uint numberOfTradesToSkip, uint numberOfTradesToReturn, SortOrder sortOrder)
        {
            NumberOfTradesToSkip = numberOfTradesToSkip;
            NumberOfTradesToReturn = numberOfTradesToReturn;
            SortOrder = sortOrder;
        }

        public uint NumberOfTradesToSkip { get; }
        public uint NumberOfTradesToReturn { get; }
        public SortOrder SortOrder { get; }

        public static SkipTakeFilter Default => new SkipTakeFilter(0, 1000, SortOrder.Desc);
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public sealed class TradeHistoryCollection : ReadOnlyCollection<TradeHistoryItem>
    {
        internal TradeHistoryCollection(IList<TradeHistoryItem> list) : base(list)
        {
        }

        public static TradeHistoryCollection Empty => new TradeHistoryCollection(new List<TradeHistoryItem>());

        internal static TradeHistoryCollection FromJsonObject(JObject jsonObject)
        {
            var historyItems = new List<TradeHistoryItem>(50);
            if (jsonObject == null) return new TradeHistoryCollection(historyItems);

            foreach (var kv in jsonObject)
            {
                var jtoken = kv.Value;
                var historyItem = new TradeHistoryItem(
                    long.Parse(kv.Key),
                    new BtcePair(jtoken.Value<string>("pair")),
                    Utils.ConvertToTradeType(jtoken.Value<string>("type")),
                    jtoken.Value<decimal>("amount"),
                    jtoken.Value<decimal>("rate"),
                    jtoken.Value<long>("order_id"),
                    jtoken.Value<int>("is_your_order") == 1,
                    UnixTime.ToLocalDateTime(jtoken.Value<uint>("timestamp")));

                historyItems.Add(historyItem);
            }

            return new TradeHistoryCollection(historyItems);
        }
    }
}
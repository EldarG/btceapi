﻿using System;
using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public sealed class Stats : FundsResponseBase
    {
        private Stats(JObject jObject) : base(jObject)
        {
        }

        public ApiKeyRights ApiKeyRights { get; private set; }
        public DateTime ServerTime { get; private set; }
        public int OpenOrdersTotal { get; private set; }

        internal static Stats FromJsonObject(JObject jsonObject)
        {
            var stats = new Stats(jsonObject);
            if (jsonObject["rights"] != null)
            {
                var rights = (JObject)jsonObject["rights"];
                if (rights["info"].Value<int>() == 1)
                {
                    stats.ApiKeyRights |= ApiKeyRights.Info;
                }
                if (rights["trade"].Value<int>() == 1)
                {
                    stats.ApiKeyRights |= ApiKeyRights.Trade;
                }
                if (rights["withdraw"].Value<int>() == 1)
                {
                    stats.ApiKeyRights |= ApiKeyRights.Withdraw;
                }
            }
            stats.ServerTime = UnixTime.ToUtcDateTime(jsonObject.Value<uint>("server_time")).ToLocalTime();
            stats.OpenOrdersTotal = jsonObject.Value<int>("open_orders");

            return stats;
        }
    }
}
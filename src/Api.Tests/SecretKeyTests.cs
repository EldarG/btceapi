﻿using System;
using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class SecretKeyTests
    {
        [Test]
        public void Compare_TwoDifferentSecretKeys_ReturnsFalse()
        {
            Assert.That(new SecretKey("secretkey1"), Is.Not.EqualTo(new SecretKey("secretkey2")));
        }

        [Test]
        public void Compare_TwoIdenticalSecretKeys_ReturnsTrue()
        {
            Assert.That(new SecretKey("secretkey"), Is.EqualTo(new SecretKey("secretkey")));
        }

        [Test]
        public void Ctor_GivenNullValue_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new SecretKey(null));
        }
    }
}
﻿using System;
using System.Text;

namespace Api
{
    public class SecretKey : IEquatable<SecretKey>
    {
        public SecretKey(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            Value = value;
        }

        public string Value { get; }

        public bool Equals(SecretKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Value, other.Value, StringComparison.InvariantCultureIgnoreCase);
        }

        public byte[] ToByteArray()
        {
            return Encoding.ASCII.GetBytes(Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(SecretKey)) return false;
            return Equals((SecretKey)obj);
        }

        public override int GetHashCode()
        {
            return Value != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(Value) : 0;
        }

        public static bool operator ==(SecretKey left, SecretKey right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SecretKey left, SecretKey right)
        {
            return !Equals(left, right);
        }
    }
}
﻿using System.Collections.Generic;
using Api.PublicApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.PublicApiDataTests
{
    [TestFixture]
    public class ApiInfoJsonMapTests
    {
        private static Dictionary<BtcePair, PairInfo> ExpectedPairs()
        {
            return new Dictionary<BtcePair, PairInfo>
                   {
                       {new BtcePair("btc_usd"), new PairInfo(3, 0.2M, false, 0.1M, 400, 0.01M)},
                       {new BtcePair("ltc_usd"), new PairInfo(5, 0.25M, false, 10.99M, 11400, 0.199M)},
                       {new BtcePair("xmp_btc"), new PairInfo(0, 1.25M, true, 0.00001M, 0.5M, 1000.999M)}
                   };
        }


        private static string Json()
        {
            var json = @"{
                            'server_time':1370814956,
                            'pairs':{
                                'btc_usd':{
                                    'decimal_places':3,
                                    'min_price':0.1,
                                    'max_price':400,
                                    'min_amount':0.01,
                                    'hidden':0,
                                    'fee':0.2        
                                    },
                                'ltc_usd':{
                                    'decimal_places':5,
                                    'min_price':10.99,
                                    'max_price':11400,
                                    'min_amount':0.199,
                                    'hidden':0,
                                    'fee':0.25        
                                    },
                            'xmp_btc':{
                                    'decimal_places':0,
                                    'min_price':0.00001,
                                    'max_price':0.5,
                                    'min_amount':1000.999,
                                    'hidden':1,
                                    'fee':1.25        
                                    }		
                            }
                        }";

            return json;
        }

        [Test]
        public void CurrencyPairsMapTest()
        {
            var apiInfo = ApiInfo.FromJsonObject(JObject.Parse(Json()));
            Assert.That(apiInfo.Pairs, Is.EquivalentTo(ExpectedPairs()));
        }

        [Test]
        public void Indexer_ShouldReturn_PairInfo()
        {
            var apiInfo = ApiInfo.FromJsonObject(JObject.Parse(Json()));
            var expected = ExpectedPairs();
            Assert.That(apiInfo["btc_usd"], Is.EqualTo(expected[new BtcePair("btc_usd")]));
        }

        [Test]
        public void ServerTimeMapTest()
        {
            var apiInfo = ApiInfo.FromJsonObject(JObject.Parse(Json()));
            Assert.That(apiInfo.ServerTime, Is.EqualTo(UnixTime.ToUtcDateTime(1370814956).ToLocalTime()));
        }
    }
}
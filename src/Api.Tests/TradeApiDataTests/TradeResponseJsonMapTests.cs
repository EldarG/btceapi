﻿using Api.TradeApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.TradeApiDataTests
{
    [TestFixture]
    public class TradeResponseJsonMapTests
    {
        [Test]
        public void FromJsonStringTest()
        {
            var json = @"{
                            'received':0.1,
                            'remains':0.5,
                            'order_id':112233,
                            'funds':{
                                'usd':325,
                                'btc':2.498,
                                'ltc':1000,			                
                            }                            
                        }";

            var response = TradeResponse.FromJsonObject(JObject.Parse(json));
            Assert.AreEqual(112233, response.OrderId);
            Assert.AreEqual(0.1M, response.Received);
            Assert.AreEqual(0.5M, response.Remains);
            Assert.AreEqual(response["usd"], 325);
            Assert.AreEqual(response["btc"], 2.498M);
            Assert.AreEqual(response["ltc"], 1000);
        }
    }
}
﻿using System;
using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public sealed class OrderInfo : IEquatable<OrderInfo>
    {
        private static readonly OrderInfo EmptyOrderInfo = new OrderInfo();

        private OrderInfo()
        {
        }

        public OrderInfo(long orderId, BtcePair btcePair, TradeType tradeType, decimal initialAmount,
            decimal remainedAmount, decimal price, OrderStatus status, DateTime createdAt)
        {
            OrderId = orderId;
            BtcePair = btcePair;
            TradeType = tradeType;
            InitialAmount = initialAmount;
            RemainedAmount = remainedAmount;
            Price = price;
            OrderStatus = status;
            CreatedAt = createdAt;
        }

        public long OrderId { get; }
        public BtcePair BtcePair { get; }
        public TradeType TradeType { get; }
        public decimal InitialAmount { get; }
        public decimal RemainedAmount { get; }
        public decimal Price { get; }
        public OrderStatus OrderStatus { get; }
        public DateTime CreatedAt { get; }

        public bool Equals(OrderInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return OrderId == other.OrderId && BtcePair.Equals(other.BtcePair) && TradeType == other.TradeType &&
                   InitialAmount == other.InitialAmount && RemainedAmount == other.RemainedAmount &&
                   Price == other.Price && OrderStatus == other.OrderStatus && CreatedAt.Equals(other.CreatedAt);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is OrderInfo && Equals((OrderInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = OrderId.GetHashCode();
                hashCode = (hashCode*397) ^ BtcePair.GetHashCode();
                hashCode = (hashCode*397) ^ (int)TradeType;
                hashCode = (hashCode*397) ^ InitialAmount.GetHashCode();
                hashCode = (hashCode*397) ^ RemainedAmount.GetHashCode();
                hashCode = (hashCode*397) ^ Price.GetHashCode();
                hashCode = (hashCode*397) ^ (int)OrderStatus;
                hashCode = (hashCode*397) ^ CreatedAt.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(OrderInfo left, OrderInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(OrderInfo left, OrderInfo right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return BtcePair.FormatOrder(TradeType, InitialAmount, Price) + $" created {CreatedAt} with id: {OrderId}";
        }

        internal static OrderInfo FromJsonObject(JObject jsonObject)
        {
            if (jsonObject.First == null) return EmptyOrderInfo;

            var orderId = long.Parse(((JProperty)jsonObject.First).Name);
            var info = jsonObject.First.First;

            var orderInfo = new OrderInfo(orderId,
                new BtcePair(info.Value<string>("pair")),
                Utils.ConvertToTradeType(info.Value<string>("type")),
                info["start_amount"] != null ? info.Value<decimal>("start_amount") : info.Value<decimal>("amount"),
                info.Value<decimal>("amount"),
                info.Value<decimal>("rate"),
                (OrderStatus)info.Value<int>("status"),
                UnixTime.ToUtcDateTime(info.Value<uint>("timestamp_created")).ToLocalTime());
            return orderInfo;
        }
    }
}
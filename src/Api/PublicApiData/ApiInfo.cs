﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Api.PublicApiData
{
    public sealed class ApiInfo
    {
        private readonly Dictionary<BtcePair, PairInfo> _internalDictionary = new Dictionary<BtcePair, PairInfo>(20);

        private ApiInfo()
        {
            Pairs = new ReadOnlyDictionary<BtcePair, PairInfo>(_internalDictionary);
        }

        public DateTime ServerTime { get; private set; }
        public IReadOnlyDictionary<BtcePair, PairInfo> Pairs { get; }

        public PairInfo this[BtcePair pair] => Pairs[pair];
        public PairInfo this[string pairName] => this[new BtcePair(pairName)];

        private void AddPairInfo(BtcePair btcePair, PairInfo pairInfo)
        {
            _internalDictionary.Add(btcePair, pairInfo);
        }

        internal static ApiInfo FromJsonObject(JObject jsonObject)
        {
            var apiInfo = new ApiInfo
                          {
                              ServerTime = UnixTime.ToUtcDateTime(jsonObject.Value<uint>("server_time")).ToLocalTime()
                          };

            if (jsonObject["pairs"] == null) return apiInfo;

            foreach (var kv in (JObject)jsonObject["pairs"])
            {
                var pairInfo = new PairInfo(
                    kv.Value.Value<int>("decimal_places"),
                    kv.Value.Value<decimal>("fee"),
                    kv.Value.Value<int>("hidden") == 1,
                    kv.Value.Value<decimal>("min_price"),
                    kv.Value.Value<decimal>("max_price"),
                    kv.Value.Value<decimal>("min_amount"));

                apiInfo.AddPairInfo(new BtcePair(kv.Key), pairInfo);
            }

            return apiInfo;
        }
    }
}
﻿using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class BtceTradeApiClientСtorAndPropertiesTests
    {
        [Test]
        public void ApiKeySetter_GivenNull_Throws()
        {
            var tradeApiClient = new BtceTradeApiClient(new ApiKey("firstValue"), new SecretKey("secretKey"));
            Assert.That(() => tradeApiClient.ApiKey = null, Throws.ArgumentNullException.With.Message.Contains("ApiKey"));
        }

        [Test]
        public void ApiKeySetter_GivenValidValue_GetReturnsThisValue()
        {
            var tradeApiClient = new BtceTradeApiClient(new ApiKey("firstValue"), new SecretKey("secretKey"));
            tradeApiClient.ApiKey = new ApiKey("newValue");
            Assert.AreEqual(new ApiKey("newValue"), tradeApiClient.ApiKey);
        }

        [Test]
        public void Ctor_NoParams_WillUseDefaultApiHostAddress()
        {
            var tradeApiClient = new BtceTradeApiClient(new ApiKey("key"), new SecretKey("key"));
            Assert.AreEqual("https://btc-e.com", tradeApiClient.ApiHostAddress);
        }

        [Test]
        public void Ctor_NullApiKeyPassed_Throws()
        {
            Assert.That(() => new BtceTradeApiClient(null, new SecretKey("notnull")),
                Throws.ArgumentNullException.With.Message.Contains("ApiKey"));
        }

        [Test]
        public void Ctor_NullSecretKeyPassed_Throws()
        {
            Assert.That(() => new BtceTradeApiClient(new ApiKey("validKey"), null),
                Throws.ArgumentNullException.With.Message.Contains("SecretKey"));
        }

        [Test]
        public void Ctor_PassApiHostAddress_ChangesApiHostAddress()
        {
            var tradeApiClient = new BtceTradeApiClient(new ApiKey("key"), new SecretKey("key"), "http://example.com");
            Assert.AreEqual(@"http://example.com", tradeApiClient.ApiHostAddress);
        }

        [Test]
        public void SecretKeySetter_GivenNull_Throws()
        {
            var tradeApiClient = new BtceTradeApiClient(new ApiKey("firstValue"), new SecretKey("secretKey"));
            Assert.That(() => tradeApiClient.SecretKey = null,
                Throws.ArgumentNullException.With.Message.Contains("SecretKey"));
        }

        [Test]
        public void SecretKeySetter_GivenValidValue_GetReturnsThisValue()
        {
            var tradeApiClient = new BtceTradeApiClient(new ApiKey("validValue"), new SecretKey("firstSecretKey"));
            tradeApiClient.SecretKey = new SecretKey("newValue");
            Assert.AreEqual(new SecretKey("newValue"), tradeApiClient.SecretKey);
        }
    }
}
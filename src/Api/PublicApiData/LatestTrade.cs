﻿using System;

namespace Api.PublicApiData
{
    public sealed class LatestTrade : IEquatable<LatestTrade>
    {
        internal LatestTrade()
        {
        }

        public TradeType TradeType { get; internal set; }
        public decimal Price { get; internal set; }
        public decimal Amount { get; internal set; }
        public long TradeId { get; internal set; }
        public DateTime TradeDateTime { get; internal set; }

        public bool Equals(LatestTrade other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return TradeType == other.TradeType && Price == other.Price && Amount == other.Amount &&
                   TradeId == other.TradeId && TradeDateTime.Equals(other.TradeDateTime);
        }

        public override string ToString()
        {
            return new {TradeType, Price, Amount, TradeId, TradeDateTime}.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is LatestTrade && Equals((LatestTrade)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)TradeType;
                hashCode = (hashCode*397) ^ Price.GetHashCode();
                hashCode = (hashCode*397) ^ Amount.GetHashCode();
                hashCode = (hashCode*397) ^ TradeId.GetHashCode();
                hashCode = (hashCode*397) ^ TradeDateTime.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(LatestTrade left, LatestTrade right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LatestTrade left, LatestTrade right)
        {
            return !Equals(left, right);
        }
    }
}
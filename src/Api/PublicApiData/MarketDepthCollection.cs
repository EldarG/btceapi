﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Api.PublicApiData
{
    public sealed class MarketDepthCollection : ReadOnlyCollection<MarketDepthCollection.MarketDepth>
    {
        internal MarketDepthCollection(IList<MarketDepth> list) : base(list)
        {
        }

        public MarketDepth this[BtcePair pair]
        {
            get { return Items.Single(x => x.BtcePair.Equals(pair)); }
        }

        public MarketDepth this[string pairName] => this[new BtcePair(pairName)];

        internal static MarketDepthCollection FromJsonObject(JObject jsonObject)
        {
            var marketDepths = new List<MarketDepth>();
            foreach (var kv in jsonObject)
            {
                var asks =
                    kv.Value["asks"].Select(x => new OrderItem(x.Value<decimal>(0), x.Value<decimal>(1))).ToArray();
                var bids =
                    kv.Value["bids"].Select(x => new OrderItem(x.Value<decimal>(0), x.Value<decimal>(1))).ToArray();
                marketDepths.Add(new MarketDepth(new BtcePair(kv.Key), asks, bids));
            }

            return new MarketDepthCollection(marketDepths);
        }

        public sealed class MarketDepth
        {
            internal MarketDepth(BtcePair btcePair, IList<OrderItem> asks, IList<OrderItem> bids)
            {
                BtcePair = btcePair;
                Asks = new ReadOnlyCollection<OrderItem>(asks);
                Bids = new ReadOnlyCollection<OrderItem>(bids);
            }

            public BtcePair BtcePair { get; }
            public IReadOnlyCollection<OrderItem> Asks { get; }
            public IReadOnlyCollection<OrderItem> Bids { get; }
        }

        public sealed class OrderItem : IEquatable<OrderItem>
        {
            public OrderItem(decimal price, decimal amount)
            {
                Amount = amount;
                Price = price;
            }

            public decimal Amount { get; }
            public decimal Price { get; }

            public bool Equals(OrderItem other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Amount == other.Amount && Price == other.Price;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                return obj is OrderItem && Equals((OrderItem)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (Amount.GetHashCode()*397) ^ Price.GetHashCode();
                }
            }

            public static bool operator ==(OrderItem left, OrderItem right)
            {
                return Equals(left, right);
            }

            public static bool operator !=(OrderItem left, OrderItem right)
            {
                return !Equals(left, right);
            }
        }
    }
}
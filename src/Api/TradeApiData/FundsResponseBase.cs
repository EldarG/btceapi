﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public abstract class FundsResponseBase
    {
        protected FundsResponseBase(JObject jObject)
        {
            var fundsDictionary = new Dictionary<string, decimal>(20, StringComparer.OrdinalIgnoreCase);
            var funds = (JObject)jObject["funds"];
            if (funds != null)
            {
                foreach (var kv in funds)
                {
                    fundsDictionary.Add(kv.Key, kv.Value.Value<decimal>());
                }
            }
            Funds = new ReadOnlyDictionary<string, decimal>(fundsDictionary);
        }

        public IReadOnlyDictionary<string, decimal> Funds { get; }
        public decimal this[string currency] => Funds[currency];
    }
}
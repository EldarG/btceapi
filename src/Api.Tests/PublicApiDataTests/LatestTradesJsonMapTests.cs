﻿using System.Collections.Generic;
using Api.PublicApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.PublicApiDataTests
{
    [TestFixture]
    public class LatestTradesJsonMapTests : AssertionHelper
    {
        private LatestTradeCollection ExpectedLatesTrades()
        {
            var btcUsdTrades = new List<LatestTrade>
                               {
                                   new LatestTrade
                                   {
                                       TradeType = TradeType.Buy,
                                       Price = 431.192M,
                                       Amount = 0.011038M,
                                       TradeId = 66483444,
                                       TradeDateTime = UnixTime.ToUtcDateTime(1452765749).ToLocalTime()
                                   },
                                   new LatestTrade
                                   {
                                       TradeType = TradeType.Sell,
                                       Price = 431.101M,
                                       Amount = 0.0506074M,
                                       TradeId = 66483439,
                                       TradeDateTime = UnixTime.ToUtcDateTime(1452765728).ToLocalTime()
                                   }
                               };
            var ltcUsdTrades = new List<LatestTrade>
                               {
                                   new LatestTrade
                                   {
                                       TradeType = TradeType.Sell,
                                       Price = 3.48M,
                                       Amount = 2,
                                       TradeId = 66483445,
                                       TradeDateTime = UnixTime.ToUtcDateTime(1452765768).ToLocalTime()
                                   },
                                   new LatestTrade
                                   {
                                       TradeType = TradeType.Buy,
                                       Price = 3.41M,
                                       Amount = 0.1998M,
                                       TradeId = 66483408,
                                       TradeDateTime = UnixTime.ToUtcDateTime(1452765710).ToLocalTime()
                                   }
                               };

            var latestTradeCollection = new LatestTradeCollection();
            latestTradeCollection.Add(new BtcePair("btc_usd"), btcUsdTrades);
            latestTradeCollection.Add(new BtcePair("ltc_usd"), ltcUsdTrades);

            return latestTradeCollection;
        }


        private static string Json()
        {
            return
                @"{ 
                    'btc_usd':
                            [
                                {'type':'bid','price':431.192,'amount':0.011038,'tid':66483444,'timestamp':1452765749},
                                {'type':'ask','price':431.101,'amount':0.0506074,'tid':66483439,'timestamp':1452765728}
                            ],
                    'ltc_usd':
                            [
                                {'type':'ask','price':3.48,'amount':2,'tid':66483445,'timestamp':1452765768},
                                {'type':'bid','price':3.41,'amount':0.1998,'tid':66483408,'timestamp':1452765710}
                            ]
                }";
        }

        [Test]
        public void LatesTradesMapsCorrectly()
        {
            var actual = LatestTradeCollection.FromJsonObject(JObject.Parse(Json()));
            var expected = ExpectedLatesTrades();

            Assert.That(actual["btc_usd"], Is.EquivalentTo(expected["btc_usd"]));
            Assert.That(actual["ltc_usd"], Is.EquivalentTo(expected["ltc_usd"]));
        }
    }
}
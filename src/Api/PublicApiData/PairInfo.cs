﻿using System;

namespace Api.PublicApiData
{
    public sealed class PairInfo : IEquatable<PairInfo>
    {
        public PairInfo(int decimalPlaces, decimal fee, bool hidden, decimal minPrice,
            decimal maxPrice, decimal minAmount)
        {
            DecimalPlaces = decimalPlaces;
            Fee = fee/100;
            Hidden = hidden;
            MaxPrice = maxPrice;
            MinAmount = minAmount;
            MinPrice = minPrice;
        }

        public int DecimalPlaces { get; }
        public decimal Fee { get; }

        public bool Hidden { get; }
        public decimal MaxPrice { get; }
        public decimal MinAmount { get; }
        public decimal MinPrice { get; }

        public bool Equals(PairInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return DecimalPlaces == other.DecimalPlaces && Fee == other.Fee && Hidden == other.Hidden &&
                   MaxPrice == other.MaxPrice && MinAmount == other.MinAmount && MinPrice == other.MinPrice;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is PairInfo && Equals((PairInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = DecimalPlaces;
                hashCode = (hashCode*397) ^ Fee.GetHashCode();
                hashCode = (hashCode*397) ^ Hidden.GetHashCode();
                hashCode = (hashCode*397) ^ MaxPrice.GetHashCode();
                hashCode = (hashCode*397) ^ MinAmount.GetHashCode();
                hashCode = (hashCode*397) ^ MinPrice.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(PairInfo left, PairInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PairInfo left, PairInfo right)
        {
            return !Equals(left, right);
        }


        public override string ToString()
        {
            return new {DecimalPlaces, Fee, MaxPrice, MinPrice, MinAmount, Hidden}.ToString();
        }
    }
}
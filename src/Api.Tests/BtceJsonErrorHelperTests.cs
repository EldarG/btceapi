﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class BtceJsonErrorHelperTests
    {
        [Test]
        public void ThrowIfError_WhenError_Throws_WithDefaultMessage_IfNotProvided()
        {
            var json = JObject.Parse(@"{ 'success':0}");

            Assert.Throws<BtceException>(() => json.ThrowIfBtceError(), "Unknown error.");
        }

        [Test]
        public void ThrowIfError_WhenError_Throws_WithMessage_ProvidedInJson()
        {
            var json = JObject.Parse(@"{ 'success':0, 'error':'Invalid pair name: btc_us'}");

            Assert.Throws<BtceException>(() => json.ThrowIfBtceError(), "Invalid pair name: btc_us");
        }

        [Test]
        public void ThrowIfError_WhenSuccess_DoesntThrow()
        {
            var json = JObject.Parse(@"{ 'success':1}");

            Assert.DoesNotThrow(() => json.ThrowIfBtceError());
        }

        [Test]
        public void ThrowIfError_WhenUnknownStatusOfOperation_DoesntNotThrow()
        {
            var json = JObject.Parse(@"{ 'data': 7}");

            Assert.DoesNotThrow(() => json.ThrowIfBtceError());
        }
    }
}
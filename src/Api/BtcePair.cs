﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Api
{
    public sealed class BtcePair : IEquatable<BtcePair>
    {
        private static readonly Regex BtcePairRegex = new Regex(@"^([a-zA-Z]{3,5})_([a-zA-Z]{3,5})$");

        public BtcePair(string pairName)
        {
            if (pairName == null)
                throw new ArgumentNullException(nameof(pairName));

            var match = BtcePairRegex.Match(pairName);

            if (!match.Success)
                throw new FormatException("Invalid pair name format.");

            Base = match.Groups[1].Value.ToUpperInvariant();
            Quote = match.Groups[2].Value.ToUpperInvariant();
        }

        public string Base { get; }
        public string Quote { get; }

        public bool Equals(BtcePair other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Base, other.Base, StringComparison.InvariantCultureIgnoreCase) &&
                   string.Equals(Quote, other.Quote, StringComparison.InvariantCultureIgnoreCase);
        }

        public string ToBtceFormat()
        {
            return (Base + "_" + Quote).ToLowerInvariant();
        }

        public override string ToString()
        {
            return Base + "_" + Quote;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(BtcePair)) return false;
            return Equals((BtcePair)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (StringComparer.InvariantCultureIgnoreCase.GetHashCode(Base)*397) ^
                       StringComparer.InvariantCultureIgnoreCase.GetHashCode(Quote);
            }
        }

        public static bool operator ==(BtcePair left, BtcePair right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BtcePair left, BtcePair right)
        {
            return !Equals(left, right);
        }

        public static explicit operator BtcePair(string pairName)
        {
            return new BtcePair(pairName);
        }

        public static implicit operator string(BtcePair btcePair)
        {
            return btcePair.ToString();
        }

        public string FormatOrder(TradeType tradeType, decimal amount, decimal price)
        {
            return string.Format("{0} {1} {2} for {3} {4}", tradeType, amount.ToString(CultureInfo.InvariantCulture),
                Base,
                price, Quote);
        }
    }
}
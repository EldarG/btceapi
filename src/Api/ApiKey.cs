﻿using System;

namespace Api
{
    public class ApiKey : IEquatable<ApiKey>
    {
        public ApiKey(string value)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string Value { get; }

        public bool Equals(ApiKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Value, other.Value, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(ApiKey)) return false;
            return Equals((ApiKey)obj);
        }

        public override int GetHashCode()
        {
            return StringComparer.InvariantCultureIgnoreCase.GetHashCode(Value);
        }

        public override string ToString()
        {
            return Value;
        }

        public static bool operator ==(ApiKey left, ApiKey right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ApiKey left, ApiKey right)
        {
            return !Equals(left, right);
        }
    }
}
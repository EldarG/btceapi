﻿using Api.TradeApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.TradeApiDataTests
{
    [TestFixture]
    public class StatsJsonMapTests
    {
        private static string Json()
        {
            var json = @"{
                            'funds':{
                                'usd':325,
                                'btc':23.998,
                                'ltc':0,			                        
                            },
                            'rights':{
                                'info':1,
                                'trade':0,
                                'withdraw':1
                            },
                            'transaction_count':0,
                            'open_orders':657,
                            'server_time':1342123547
                        }";
            return json;
        }

        [Test]
        public void ApiKeyRightsMapTest()
        {
            var stats = Stats.FromJsonObject(JObject.Parse(Json()));
            Assert.IsTrue(stats.ApiKeyRights.HasFlag(ApiKeyRights.Info));
            Assert.IsFalse(stats.ApiKeyRights.HasFlag(ApiKeyRights.Trade));
            Assert.IsTrue(stats.ApiKeyRights.HasFlag(ApiKeyRights.Withdraw));
        }

        [Test]
        public void FundsMappingTest()
        {
            var stats = Stats.FromJsonObject(JObject.Parse(Json()));

            Assert.AreEqual(stats.Funds["usd"], 325);
            Assert.AreEqual(stats.Funds["btc"], 23.998M);
            Assert.AreEqual(stats.Funds["ltc"], 0);
        }

        [Test]
        public void ServerTimeMapTest()
        {
            var stats = Stats.FromJsonObject(JObject.Parse(Json()));
            Assert.AreEqual(UnixTime.ToUtcDateTime(1342123547).ToLocalTime(), stats.ServerTime);
        }

        [Test]
        public void TotatlOpenOrdersMapTest()
        {
            var stats = Stats.FromJsonObject(JObject.Parse(Json()));
            Assert.AreEqual(657, stats.OpenOrdersTotal);
        }
    }
}
﻿using System.Threading;

namespace Api
{
    public static class Nonce
    {
        private static long _nonce = UnixTime.Now;
        public static long Value => Interlocked.Increment(ref _nonce);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Api.Tests
{
    internal class TestableBtceTradeApiClient : BtceTradeApiClient
    {
        private readonly FakeHttpMessageHandler _handler;

        public TestableBtceTradeApiClient(ApiKey apiKey, SecretKey secretKey, string apiHostWithProtocol = null)
            : base(apiKey, secretKey, apiHostWithProtocol)
        {
            _handler = new FakeHttpMessageHandler();
        }

        public HttpRequestMessage LatestRequest => _handler.LatestRequest;
        public Dictionary<string, string> LatestPostData => _handler.PostData.ToDictionary(x => x.Key, x => x.Value);

        public Func<long> NonceFactory { get; set; }

        public void MockHttpResponse(HttpResponseMessage httpResponseMessage)
        {
            _handler.SetHttpResponse(httpResponseMessage);
        }

        public void MockHttpResponse(string content, HttpStatusCode httpStatusCode)
        {
            var response = new HttpResponseMessage
                           {
                               StatusCode = httpStatusCode,
                               Content = new StringContent(content)
                           };

            MockHttpResponse(response);
        }

        public void MockSuccessResponse()
        {
            var response = new HttpResponseMessage
                           {
                               StatusCode = HttpStatusCode.OK,
                               Content = new StringContent("{success: 1, return: {}}")
                           };

            MockHttpResponse(response);
        }

        public void MockBadResponse(string errorMessage = null)
        {
            var response = new HttpResponseMessage
                           {
                               StatusCode = HttpStatusCode.OK,
                               Content = errorMessage != null
                                   ? new StringContent("{success: 0, error: '" + errorMessage + "'}")
                                   : new StringContent("{success: 0, error: 'some error occured'}")
                           };

            MockHttpResponse(response);
        }

        protected override long GetNonce()
        {
            return NonceFactory?.Invoke() ?? base.GetNonce();
        }

        protected override HttpClient CreateHttpClient()
        {
            return new HttpClient(_handler);
        }
    }
}
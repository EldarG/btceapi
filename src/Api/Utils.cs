﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api
{
    public static class Utils
    {
        internal static TradeType ConvertToTradeType(string tradeType)
        {
            if (tradeType == null) return TradeType.Unknown;

            tradeType = tradeType.ToLowerInvariant();
            switch (tradeType)
            {
                case "bid":
                case "buy":
                    return TradeType.Buy;
                case "ask":
                case "sell":
                    return TradeType.Sell;
                default:
                    return TradeType.Unknown;
            }
        }

        internal static uint NarrowLimit(uint limit)
        {
            if (limit <= 0) limit = 150;
            if (limit >= 2000) limit = 2000;
            return limit;
        }

        internal static string HyphenizeBtcePairs(IEnumerable<BtcePair> btcePairs,
            bool atLeastOnePairRequired = false)
        {
            if (btcePairs == null)
            {
                if (atLeastOnePairRequired)
                    throw new ArgumentNullException(nameof(btcePairs));
                return string.Empty;
            }

            var sb = new StringBuilder();
            foreach (var btcePair in btcePairs)
            {
                if (btcePair != null)
                {
                    sb.Append(btcePair.ToBtceFormat());
                    sb.Append('-');
                }
            }
            if (sb.Length > 0) sb.Length--; // removes last hyphen
            if (sb.Length > 0)
            {
                sb.Insert(0, '/');
                return sb.ToString();
            }

            if (atLeastOnePairRequired)
                throw new ArgumentException("At least one btcePair required.", nameof(btcePairs));

            return string.Empty;
        }
    }
}
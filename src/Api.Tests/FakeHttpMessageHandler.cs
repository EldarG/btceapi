﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Tests
{
    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        private HttpResponseMessage _response;

        public FakeHttpMessageHandler(HttpResponseMessage response)
        {
            _response = response;
        }

        public FakeHttpMessageHandler()
        {
        }

        public HttpRequestMessage LatestRequest { get; private set; }
        public IEnumerable<KeyValuePair<string, string>> PostData { get; private set; }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            LatestRequest = request;

            if (request.Method == HttpMethod.Post)
            {
                PostData = FromQueryString(request.Content.ReadAsStringAsync().Result);
            }

            var responseTask = new TaskCompletionSource<HttpResponseMessage>();
            responseTask.SetResult(_response);

            return responseTask.Task;
        }

        private IDictionary<string, string> FromQueryString(string queryString)
        {
            if (queryString == null)
                throw new ArgumentNullException(nameof(queryString));

            if (!Uri.IsWellFormedUriString(queryString, UriKind.RelativeOrAbsolute))
                throw new ArgumentException("Value is not well formed.", nameof(queryString));

            var postParams = new Dictionary<string, string>();
            var pairs = queryString.Split('&');
            foreach (var pair in pairs)
            {
                var index2 = pair.IndexOf('=');
                if (index2 > 0)
                {
                    postParams.Add(pair.Substring(0, index2), pair.Substring(index2 + 1));
                }
            }

            return postParams;
        }

        public static FakeHttpMessageHandler Create(string content, HttpStatusCode httpStatusCode)
        {
            var response = new HttpResponseMessage
                           {
                               StatusCode = httpStatusCode,
                               Content = new StringContent(content)
                           };

            return new FakeHttpMessageHandler(response);
        }

        public void SetHttpResponse(HttpResponseMessage httpResponseMessage)
        {
            _response = httpResponseMessage;
        }
    }
}
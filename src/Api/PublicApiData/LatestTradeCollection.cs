﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Api.PublicApiData
{
    public sealed class LatestTradeCollection
    {
        private readonly Dictionary<BtcePair, IReadOnlyCollection<LatestTrade>> _internalDictionary;

        internal LatestTradeCollection()
        {
            _internalDictionary = new Dictionary<BtcePair, IReadOnlyCollection<LatestTrade>>(10);
        }

        public IReadOnlyCollection<LatestTrade> this[string pairName] => this[new BtcePair(pairName)];
        public IReadOnlyCollection<LatestTrade> this[BtcePair pair] => _internalDictionary[pair];

        internal void Add(BtcePair btcePair, IList<LatestTrade> trades)
        {
            _internalDictionary.Add(btcePair, new ReadOnlyCollection<LatestTrade>(trades));
        }

        internal static LatestTradeCollection FromJsonObject(JObject jsonObject)
        {
            var latestTrades = new LatestTradeCollection();

            foreach (var kv in jsonObject)
            {
                var trades = new List<LatestTrade>();
                foreach (var item in kv.Value)
                {
                    var trade = new LatestTrade
                                {
                                    Amount = item.Value<decimal>("amount"),
                                    Price = item.Value<decimal>("price"),
                                    TradeType = Utils.ConvertToTradeType(item.Value<string>("type")),
                                    TradeDateTime = UnixTime.ToUtcDateTime(item.Value<uint>("timestamp")).ToLocalTime(),
                                    TradeId = item.Value<long>("tid")
                                };
                    trades.Add(trade);
                }
                latestTrades.Add(new BtcePair(kv.Key), trades);
            }

            return latestTrades;
        }
    }
}
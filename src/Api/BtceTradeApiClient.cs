﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Api.TradeApiData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Api
{
    /// <summary>
    ///     This is implementation of trade API https://wex.nz/tapi/docs.
    ///     In order to use this API, you have to create api-secret keypair.
    ///     Keypair can be created in your Profile in the API Keys section.
    /// </summary>
    public class BtceTradeApiClient : BtceApiClientBase
    {
        private readonly string _apiVersionSuffix = "/tapi";
        private readonly string _tradeApiBaseUrl;
        private ApiKey _apiKey;

        private SecretKey _secretKey;

        /// <summary>
        ///     Single instance of this class during whole app lifetime is recommended approach.
        /// </summary>
        /// <param name="apiKey">your api key</param>
        /// <param name="secretKey">your secret key</param>
        /// <param name="apiHostWithProtocol">domain name with protocol http/https</param>
        public BtceTradeApiClient(ApiKey apiKey, SecretKey secretKey, string apiHostWithProtocol = @"https://wex.nz")
            : base(apiHostWithProtocol)
        {
            ApiKey = apiKey;
            SecretKey = secretKey;
            _tradeApiBaseUrl = ApiHostAddress + _apiVersionSuffix;
        }

        public ApiKey ApiKey
        {
            get { return _apiKey; }
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(ApiKey), nameof(ApiKey) + " cannot be null.");
                _apiKey = value;
            }
        }

        public SecretKey SecretKey
        {
            get { return _secretKey; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(SecretKey), nameof(SecretKey) + " cannot be null.");
                _secretKey = value;
            }
        }

        /// <summary>
        ///     Returns information about the user’s current balance, API-key privileges, the number of open orders.
        ///     Info privilege is required on API key.
        /// </summary>
        /// <returns>instance of <see cref="Stats" /></returns>
        public async Task<Stats> GetStatsAsync()
        {
            var postData = PostDataBuilder.New().WithMethod("getInfo");
            return await PostAsync(postData, Stats.FromJsonObject);
        }

        /// <summary>
        ///     Use this method to create orders. Trade privilege is required on API key.
        /// </summary>
        /// <param name="btcePair">currency pair</param>
        /// <param name="tradeType">trade type buy or sell</param>
        /// <param name="price">order price</param>
        /// <param name="amount">the amount you need to buy or sell</param>
        /// <returns></returns>
        public async Task<TradeResponse> TradeAsync(BtcePair btcePair, TradeType tradeType, Price price,
            Amount amount)
        {
            if (btcePair == null) throw new ArgumentNullException(nameof(btcePair));

            var postData = PostDataBuilder.New()
                .WithMethod("Trade")
                .WithPrice(price)
                .WithAmount(amount)
                .WithBtcePair(btcePair)
                .WithTradeType(tradeType);
            return await PostAsync(postData, TradeResponse.FromJsonObject);
        }

        /// <summary>
        ///     Returns active orders for all pairs.
        ///     To use this method you need a privilege of the key info.
        /// </summary>
        /// <exception cref="BtceException">if no orders found</exception>
        /// <returns>The readonly collection of <see cref="OrderInfo" />orderInfo.</returns>
        public async Task<ActiveOrderCollection> GetActiveOrdersAsync()
        {
            var postData = PostDataBuilder.New().WithMethod("ActiveOrders");
            return await PostAsync(postData, ActiveOrderCollection.FromJsonObject);
        }

        /// <summary>
        ///     Returns active orders for specified currency pair.
        /// </summary>
        /// <param name="btcePair">interested pair</param>
        /// <exception cref="ArgumentNullException">
        ///     if
        ///     <param name="btcePair">is null</param>
        /// </exception>
        /// <exception cref="BtceException">if no orders found</exception>
        /// <returns>The readonly collection of <see cref="OrderInfo" />orderInfo.</returns>
        public async Task<ActiveOrderCollection> GetActiveOrdersAsync(BtcePair btcePair)
        {
            if (btcePair == null)
                throw new ArgumentNullException(nameof(btcePair));

            var postData = PostDataBuilder.New()
                .WithMethod("ActiveOrders")
                .WithBtcePair(btcePair);
            return await PostAsync(postData, ActiveOrderCollection.FromJsonObject);
        }

        /// <summary>
        ///     Returns information about single order by given orderId.
        /// </summary>
        /// <param name="orderId">order identificator</param>
        /// <returns>instance of OrderInfo</returns>
        public async Task<OrderInfo> GetOrderInfoAsync(long orderId)
        {
            if (orderId <= 0)
                throw new ArgumentException("Value must be positive.", nameof(orderId));

            var postParams = PostDataBuilder.New()
                .WithMethod("OrderInfo")
                .WithData("order_id", orderId);
            return await PostAsync(postParams, OrderInfo.FromJsonObject);
        }

        /// <summary>
        ///     Cancels single order by given orderId.
        /// </summary>
        /// <param name="orderId">order identificator</param>
        /// <returns></returns>
        public async Task<CancelOrderResponse> CancelOrderAsync(long orderId)
        {
            if (orderId <= 0)
                throw new ArgumentException("Value must be positive.", nameof(orderId));

            var postParams = PostDataBuilder.New()
                .WithMethod("CancelOrder")
                .WithData("order_id", orderId);
            return await PostAsync(postParams, CancelOrderResponse.FromJsonObject);
        }

        /// <summary>
        ///     Returns trade history in skip and take manner.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="btcePair"></param>
        /// <returns></returns>
        public async Task<TradeHistoryCollection> GetTradeHistoryAsync(SkipTakeFilter filter, BtcePair btcePair = null)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));

            return
                await
                    GetTradeHistoryAsyncCore(filter.NumberOfTradesToSkip, filter.NumberOfTradesToReturn,
                        sortOrder: filter.SortOrder, btcePair: btcePair);
        }

        /// <summary>
        ///     Returns trade history, filtered by tradeId range for any or given pair.
        /// </summary>
        /// <param name="tradeIdRangeFilter"></param>
        /// <param name="btcePair">interested pair</param>
        /// <returns>Trade history collection.</returns>
        public async Task<TradeHistoryCollection> GetTradeHistoryAsync(TradeIdRangeFilter tradeIdRangeFilter,
            BtcePair btcePair = null)
        {
            if (tradeIdRangeFilter == null)
                throw new ArgumentNullException(nameof(tradeIdRangeFilter));
            return
                await
                    GetTradeHistoryAsyncCore(fromTradeId: tradeIdRangeFilter.StartFromTradeId,
                        toTradeId: tradeIdRangeFilter.EndOnTradeId, sortOrder: tradeIdRangeFilter.SortOrder,
                        nbToReturn: tradeIdRangeFilter.NumberOfTradesToReturn, btcePair: btcePair);
        }

        private async Task<TradeHistoryCollection> GetTradeHistoryAsyncCore(
            uint? nbToskip = 0,
            uint? nbToReturn = 1000,
            ulong? fromTradeId = 0,
            ulong? toTradeId = 0,
            DateTime? startDateTime = null, DateTime? endDateTime = null, SortOrder sortOrder = SortOrder.Desc,
            BtcePair btcePair = null)
        {
            var postParams = PostDataBuilder.New()
                .WithMethod("TradeHistory")
                .WithDataIfNotNull("from_id", fromTradeId)
                .WithDataIfNotNull("end_id", toTradeId)
                .WithDataIfNotNull("from", nbToskip)
                .WithDataIfNotNull("count", nbToReturn)
                .WithDataIfNotNull("since", startDateTime)
                .WithDataIfNotNull("end", endDateTime)
                .WithBtcePairIfNotNull(btcePair)
                .WithData("order", sortOrder.ToString().ToUpperInvariant());
            return await PostAsync(postParams, TradeHistoryCollection.FromJsonObject);
        }

        /// <summary>
        ///     Returns trade history, filtered by datetime range for any or given pair.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="btcePair"></param>
        /// <returns></returns>
        public async Task<TradeHistoryCollection> GetTradeHistoryAsync(DateTimeRangeFilter filter,
            BtcePair btcePair = null)
        {
            return
                await
                    GetTradeHistoryAsyncCore(startDateTime: filter.StartDateTime, endDateTime: filter.EndDateTime,
                        nbToReturn: filter.NumberOfTradesToReturn, sortOrder: SortOrder.Asc, btcePair: btcePair);
        }

        private async Task<T> PostAsync<T>(PostDataBuilder postDataBuilder, Func<JObject, T> jsonObjectMapper)
        {
            postDataBuilder.WithData("nonce", GetNonce());
            var signature = postDataBuilder.CalculateSignature(SecretKey);

            using (var request = new HttpRequestMessage(HttpMethod.Post, _tradeApiBaseUrl))
            using (var content = new FormUrlEncodedContent(postDataBuilder.ToKeyValuePairs()))
            {
                request.Headers.Add("Key", ApiKey.ToString());
                request.Headers.Add("Sign", signature);
                request.Content = content;

                using (var response =
                    await HttpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false))
                using (var stream = await response.Content.ReadAsStreamAsync())
                using (var streamReader = new StreamReader(stream))
                using (var jsonTextReader = new JsonTextReader(streamReader))
                {
                    var jObject = JObject.Load(jsonTextReader);
                    jObject.ThrowIfBtceError();
                    return jsonObjectMapper((JObject)jObject["return"]);
                }
            }
        }

        protected virtual long GetNonce()
        {
            return Nonce.Value;
        }
    }
}
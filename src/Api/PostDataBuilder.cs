﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Api
{
    internal sealed class PostDataBuilder
    {
        private readonly List<KeyValuePair<string, string>> _keyValuePairs
            = new List<KeyValuePair<string, string>>(10);

        private PostDataBuilder()
        {
        }

        public static PostDataBuilder New()
        {
            return new PostDataBuilder();
        }

        public PostDataBuilder WithMethod(string apiMethodName)
        {
            _keyValuePairs.Add("method", apiMethodName);
            return this;
        }

        public PostDataBuilder WithTradeType(TradeType tradeType)
        {
            _keyValuePairs.Add("type", tradeType.ToString().ToLowerInvariant());
            return this;
        }

        public PostDataBuilder WithBtcePair(BtcePair btcePair)
        {
            _keyValuePairs.Add("pair", btcePair.ToString().ToLowerInvariant());
            return this;
        }

        private string ToQueryString()
        {
            return string.Join("&",
                _keyValuePairs.Select(kv => $"{Uri.EscapeDataString(kv.Key)}={Uri.EscapeDataString(kv.Value)}"));
        }

        public IEnumerable<KeyValuePair<string, string>> ToKeyValuePairs()
        {
            return _keyValuePairs;
        }

        public PostDataBuilder WithPrice(Price price)
        {
            _keyValuePairs.Add("rate", price.ToString());
            return this;
        }

        public PostDataBuilder WithAmount(Amount amount)
        {
            _keyValuePairs.Add("amount", amount.ToString());
            return this;
        }

        public PostDataBuilder WithData(string key, long value)
        {
            _keyValuePairs.Add(key, value.ToString());
            return this;
        }

        private PostDataBuilder WithData(string key, ulong value)
        {
            _keyValuePairs.Add(key, value.ToString());
            return this;
        }

        public PostDataBuilder WithData(string key, string value)
        {
            _keyValuePairs.Add(key, value);
            return this;
        }

        public PostDataBuilder WithDataIfNotNull(string key, ulong? value)
        {
            if (!value.HasValue) return this;
            return WithData(key, value.Value);
        }

        public PostDataBuilder WithDataIfNotNull(string key, uint? value)
        {
            if (!value.HasValue) return this;
            return WithData(key, value.Value);
        }

        public PostDataBuilder WithDataIfNotNull(string key, DateTime? value)
        {
            if (!value.HasValue)
                return this;
            return WithData(key, value.Value);
        }

        private PostDataBuilder WithData(string key, DateTime value)
        {
            _keyValuePairs.Add(key, value.ToUnixTimestamp().ToString());
            return this;
        }

        public string CalculateSignature(SecretKey secretKey)
        {
            var queryString = ToQueryString();
            var dataBytes = Encoding.ASCII.GetBytes(queryString);
            using (var hmacsha512 = new HMACSHA512(secretKey.ToByteArray()))
            {
                var signature = hmacsha512.ComputeHash(dataBytes);
                return BitConverter.ToString(signature).Replace("-", string.Empty).ToLowerInvariant();
            }
        }

        public PostDataBuilder WithBtcePairIfNotNull(BtcePair btcePair)
        {
            if (btcePair == null)
                return this;
            return WithBtcePair(btcePair);
        }
    }

    internal static class ListOfKeyValuePairExtensions
    {
        public static void Add(this List<KeyValuePair<string, string>> keyValuePairList, string key, string value)
        {
            keyValuePairList.Add(new KeyValuePair<string, string>(key, value));
        }
    }
}
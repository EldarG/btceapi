﻿using Api.PublicApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Order = Api.PublicApiData.MarketDepthCollection.OrderItem;

namespace Api.Tests.PublicApiDataTests
{
    [TestFixture]
    public class MarketDepthJsonMapTests
    {
        private static string Json()
        {
            return @"{   
                        'btc_usd':{
                            'asks':[
                                [103.426, 0.01],
                                [103.5, 15],                                                             
                            ],
                            'bids':[
                                [103.2, 2.48502251],
                                [103.082, 0.46540304],                                                             
                            ]
                        },
                        'ltc_usd':{
                            'asks':[
                                [1.01, 10.01],
                                [1.035, 115],                                                               
                            ],
                            'bids':[
                                [1.032, 12.48502251],
                                [1.03082, 10.46540304],                                                               
                            ]
                        }
    
                    }";
        }


        private static MarketDepthCollection ExpectedMarketDepthCollection()
        {
            var btcUsdAsks = new[]
                             {
                                 new Order(103.426M, 0.01M),
                                 new Order(103.5M, 15)
                             };

            var btcUsdBids = new[]
                             {
                                 new Order(103.2M, 2.48502251M),
                                 new Order(103.082M, 0.46540304M)
                             };

            var ltcUsdAsks = new[]
                             {
                                 new Order(1.01M, 10.01M),
                                 new Order(1.035M, 115M)
                             };
            var ltcUsdBids = new[]
                             {
                                 new Order(1.032M, 12.48502251M),
                                 new Order(1.03082M, 10.46540304M)
                             };

            return new MarketDepthCollection(new[]
                                             {
                                                 new MarketDepthCollection.MarketDepth(new BtcePair("btc_usd"),
                                                     btcUsdAsks, btcUsdBids),
                                                 new MarketDepthCollection.MarketDepth(new BtcePair("ltc_usd"),
                                                     ltcUsdAsks, ltcUsdBids)
                                             });
        }

        [Test]
        public void MarketDepthMapsToObjectCorrectly()
        {
            var actual = MarketDepthCollection.FromJsonObject(JObject.Parse(Json()));
            var expected = ExpectedMarketDepthCollection();

            Assert.That(actual["btc_usd"].Asks, Has.Count.EqualTo(2).And.EquivalentTo(expected["btc_usd"].Asks));
            Assert.That(actual["btc_usd"].Bids, Has.Count.EqualTo(2).And.EquivalentTo(expected["btc_usd"].Bids));

            Assert.That(actual["ltc_usd"].Asks, Has.Count.EqualTo(2).And.EquivalentTo(expected["ltc_usd"].Asks));
            Assert.That(actual["ltc_usd"].Bids, Has.Count.EqualTo(2).And.EquivalentTo(expected["ltc_usd"].Bids));
        }
    }
}
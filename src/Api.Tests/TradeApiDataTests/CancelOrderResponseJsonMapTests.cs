﻿using Api.TradeApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.TradeApiDataTests
{
    [TestFixture]
    public class CancelOrderResponseJsonMapTests
    {
        [Test]
        public void FromJsonObjectMapsJsonCorrectly()
        {
            var json = @"{                      
                            'order_id':343154,
                            'funds':
                             {
                                'usd':325,
                                'btc':24.998,
                                'ltc':0,			            
                             }                             
                        }";

            var response = CancelOrderResponse.FromJsonObject(JObject.Parse(json));

            Assert.AreEqual(343154, response.OrderId);
            Assert.AreEqual(response["usd"], 325);
            Assert.AreEqual(response["btc"], 24.998M);
            Assert.AreEqual(response["ltc"], 0);
        }
    }
}
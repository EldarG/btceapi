using System;

namespace Api
{
    public sealed class TradeIdRangeFilter
    {
        public TradeIdRangeFilter(ulong startFromTradeId, ulong endOnTradeId, uint numberOfTradesToReturn,
            SortOrder sortOrder)
        {
            if (startFromTradeId > endOnTradeId)
            {
                throw new ArgumentOutOfRangeException(nameof(endOnTradeId),
                    $"Value of {nameof(endOnTradeId)} must be equal or greater than {nameof(startFromTradeId)}.");
            }

            StartFromTradeId = startFromTradeId;
            EndOnTradeId = endOnTradeId;
            NumberOfTradesToReturn = numberOfTradesToReturn;
            SortOrder = sortOrder;
        }

        public ulong StartFromTradeId { get; }
        public ulong EndOnTradeId { get; }
        public uint NumberOfTradesToReturn { get; }
        public SortOrder SortOrder { get; }

        public static TradeIdRangeFilter Default => new TradeIdRangeFilter(0, 0, 1000, SortOrder.Desc);
    }
}
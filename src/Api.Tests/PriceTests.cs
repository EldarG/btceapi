﻿using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class PriceTests
    {
        [Test]
        public void Compare_WithDecimal_ReturnsCorrectResult()
        {
            Assert.IsTrue(new Price(10) > new Price(9.99M));
            Assert.IsTrue(new Price(99) < new Price(99.99M));
            Assert.IsFalse(new Price(999) > new Price(999.0001M));

            Assert.IsTrue(new Price(99.11111M) <= new Price(99.2M));
            Assert.IsTrue(new Price(88.8888M) >= new Price(88.888799999M));
        }
    }
}
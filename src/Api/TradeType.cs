﻿namespace Api
{
    public enum TradeType
    {
        Unknown = 0,
        Buy = 1,
        Sell = 2
    }
}
﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Api
{
    public abstract class BtceApiClientBase
    {
        private readonly string _defaultBtceHost = "https://wex.nz";
        private readonly Lazy<HttpClient> _httpClientLazy;

        protected BtceApiClientBase(string apiHostWithProtocol)
        {
            ApiHostAddress = apiHostWithProtocol?.TrimEnd('/', '\\') ?? _defaultBtceHost;
            _httpClientLazy = new Lazy<HttpClient>(CreateHttpClient);
        }

        public string ApiHostAddress { get; }
        protected HttpClient HttpClient => _httpClientLazy.Value;

        protected async Task<T> GetJsonStreamAsync<T>(string url, Func<JObject, T> jObjectMapper)
        {
            using (var stream = await HttpClient.GetStreamAsync(new Uri(url)))
            using (var streamReader = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(streamReader))
            {
                var jObject = JObject.Load(jsonTextReader);
                jObject.ThrowIfBtceError();
                return jObjectMapper(jObject);
            }
        }

        protected virtual HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient();
            return httpClient;
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.PublicApiData;

namespace Api
{
    /// <summary>
    ///     This is implementation of public API https://wex.nz/api/3/
    ///     This api provides access to such information as tickers of currency pairs, active orders on different pairs,
    ///     the latest trades for each pair etc.
    /// </summary>
    public class BtcePublicApiClient : BtceApiClientBase
    {
        private readonly string _apiPartSuffix = "/api/3";
        private readonly string _publicApiBaseUrl;

        /// <summary>
        ///     Single instance is recommended during whole app lifetime.
        /// </summary>
        /// <param name="apiHostWithProtocol">domain name with protocol http/https</param>
        public BtcePublicApiClient(string apiHostWithProtocol = @"https://wex.nz") : base(apiHostWithProtocol)
        {
            _publicApiBaseUrl = ApiHostAddress + _apiPartSuffix;
        }

        /// <summary>Provides information about currently active pairs, price and amount decimal precisions, min-max prices.</summary>
        /// <returns><see cref="ApiInfo" /> instance.</returns>
        public async Task<ApiInfo> GetApiInfoAsync()
        {
            var url = _publicApiBaseUrl + "/info";
            return await GetJsonStreamAsync(url, ApiInfo.FromJsonObject);
        }

        /// <summary>
        ///     Provides information over the past 24 hours about max/min price, average price,
        ///     trade volume, trade volume in currency, the last trade, Buy and Sell price.
        /// </summary>
        /// <param name="btcePairs">interested pair or pairs</param>
        /// <returns>Readonly collection of <see cref="Ticker" /> items.</returns>
        public async Task<TickerDictionary> GetTickerAsync(params BtcePair[] btcePairs)
        {
            var url = $"{_publicApiBaseUrl}/ticker";
            url += Utils.HyphenizeBtcePairs(btcePairs);
            return await GetJsonStreamAsync(url, TickerDictionary.FromJsonObject);
        }

        /// <summary>Provides information about active orders on the pairs.</summary>
        /// <param name="btcePairs">interested pairs, at least one pair is required</param>
        /// <param name="limit">number of orders to return, default is 150, max is 2000</param>
        /// <returns>Readonly collection of <see cref="MarketDepthCollection.MarketDepth" /> items.</returns>
        public async Task<MarketDepthCollection> GetMarketDepthAsync(IEnumerable<BtcePair> btcePairs, uint limit = 150)
        {
            string url = $"{_publicApiBaseUrl}/depth";
            url += Utils.HyphenizeBtcePairs(btcePairs, true);
            limit = Utils.NarrowLimit(limit);
            url += $"?limit={limit}";
            return await GetJsonStreamAsync(url, MarketDepthCollection.FromJsonObject);
        }

        /// <summary>Provides information about active orders on specified pair.</summary>
        /// <param name="btcePair">interested pair</param>
        /// <param name="limit">number of orders to return, default is 150, max is 2000</param>
        /// <returns>Readonly collection of <see cref="MarketDepthCollection.MarketDepth" /> items.</returns>
        public async Task<MarketDepthCollection> GetMarketDepthAsync(BtcePair btcePair, uint limit = 150)
        {
            return await GetMarketDepthAsync(new[] {btcePair}, limit);
        }

        /// <summary>Provides information about latest trades.</summary>
        /// <param name="btcePairs">interested pairs, at least one pair is required</param>
        /// <param name="limit">number of trades to return, default is 150, maximum 2000</param>
        /// <returns>Readonly collection of <see cref="LatestTrade" /> items.</returns>
        public async Task<LatestTradeCollection> GetLatestTradesAsync(IEnumerable<BtcePair> btcePairs, uint limit = 150)
        {
            string url = $"{_publicApiBaseUrl}/trades";
            url += Utils.HyphenizeBtcePairs(btcePairs, true);
            limit = Utils.NarrowLimit(limit);
            url += $"?limit={limit}";
            return await GetJsonStreamAsync(url, LatestTradeCollection.FromJsonObject);
        }

        /// <summary>Provides information about latest trades on specified pair.</summary>
        /// <param name="btcePair">interested pair</param>
        /// <param name="limit">number of trades to return, default is 150, maximum 2000</param>
        /// <returns>Readonly collection of <see cref="LatestTrade" /> items.</returns>
        public async Task<LatestTradeCollection> GetLatestTradesAsync(BtcePair btcePair, uint limit = 150)
        {
            return await GetLatestTradesAsync(new[] {btcePair}, limit);
        }
    }
}
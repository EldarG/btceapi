﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Api.PublicApiData
{
    public class TickerDictionary : ReadOnlyDictionary<BtcePair, Ticker>
    {
        internal TickerDictionary(IDictionary<BtcePair, Ticker> dictionary) : base(dictionary)
        {
        }

        internal static TickerDictionary FromJsonObject(JObject jsonObject)
        {
            var tickers = new Dictionary<BtcePair, Ticker>(10);
            foreach (var kv in jsonObject)
            {
                var btcePair = new BtcePair(kv.Key);
                var ticker = new Ticker(
                    kv.Value.Value<decimal>("high"),
                    kv.Value.Value<decimal>("low"),
                    kv.Value.Value<decimal>("avg"),
                    kv.Value.Value<decimal>("vol"),
                    kv.Value.Value<decimal>("vol_cur"),
                    kv.Value.Value<decimal>("last"),
                    kv.Value.Value<decimal>("buy"),
                    kv.Value.Value<decimal>("sell"),
                    UnixTime.ToUtcDateTime(kv.Value.Value<uint>("updated")).ToLocalTime());

                tickers.Add(btcePair, ticker);
            }

            return new TickerDictionary(tickers);
        }
    }
}
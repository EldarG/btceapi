﻿namespace Api
{
    public enum OrderStatus
    {
        Active = 0,
        Fullfilled = 1,
        Cancelled = 2,
        PartiallyCancelled = 3
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace Api
{
    [Serializable]
    public class BtceException : Exception
    {
        public BtceException()
        {
        }

        public BtceException(string message) : base(message)
        {
        }

        public BtceException(string message, Exception inner) : base(message, inner)
        {
        }

        protected BtceException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
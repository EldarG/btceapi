﻿using Api.TradeApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.TradeApiDataTests
{
    [TestFixture]
    public class TradeHistoryJsonMapTests
    {
        private string Json()
        {
            return @"{ 
                       '166830':{
                            'pair':'btc_usd',
                            'type':'sell',
                            'amount':1,
                            'rate':450,
                            'order_id':343148,
                            'is_your_order':1,
                            'timestamp':1342445793

                       },
                       '166831':{
                            'pair':'btc_usd',
                            'type':'buy',
                            'amount':1.01,
                            'rate':460.11,
                            'order_id':343149,
                            'is_your_order':1,
                            'timestamp':1342445794

                       },
                       '166832':{
                            'pair':'ltc_usd',
                            'type':'sell',
                            'amount':1000.567,
                            'rate':3.01,
                            'order_id':343150,
                            'is_your_order':0,
                            'timestamp':1342445795

                       },
                    }";
        }


        private TradeHistoryCollection ExpectedTradesCollection()
        {
            var trades = new[]
                         {
                             new TradeHistoryItem(166830, new BtcePair("btc_usd"), TradeType.Sell, 1, 450, 343148, true,
                                 UnixTime.ToLocalDateTime(1342445793)),
                             new TradeHistoryItem(166831, new BtcePair("btc_usd"), TradeType.Buy, 1.01M, 460.11M, 343149,
                                 true,
                                 UnixTime.ToLocalDateTime(1342445794)),
                             new TradeHistoryItem(166832, new BtcePair("ltc_usd"), TradeType.Sell, 1000.567M, 3.01M,
                                 343150,
                                 false,
                                 UnixTime.ToLocalDateTime(1342445795))
                         };

            return new TradeHistoryCollection(trades);
        }

        [Test]
        public void EmptyResultShouldCreateEmptyCollection()
        {
            var actual = new TradeHistoryCollection(new TradeHistoryItem[0]);
            Assert.That(actual, Is.Empty);
        }


        [Test]
        public void FromJsonObjectShouldMapCorrectly()
        {
            var actual = TradeHistoryCollection.FromJsonObject(JObject.Parse(Json()));
            var expected = ExpectedTradesCollection();
            Assert.That(actual, Has.Count.EqualTo(3));
            Assert.That(actual, Is.EquivalentTo(expected));
        }
    }
}
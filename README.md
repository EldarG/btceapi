.NET api for https://wex.nz (former btc-e.com) exchange. Api uses modern HttpClient in async/await way.



Examples. You can find more examples in unit tests.

```
#!c#
// access to public api methods
var publicApiClient = new BtcePublicApiClient();

// get market depth for btc_usd
await publicApiClient.GetMarketDepthAsync(new BtcePair("btc_usd"));



// acess to trade api methods
var tradeApiClient = new BtceTradeApiClient(new ApiKey("yourApiKey"), new Secret("yourSecretKey"));
```
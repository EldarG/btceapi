﻿using System.Net;
using System.Net.Http;

namespace Api.Tests.PublicApiDataTests
{
    public class TestableBtcePublicApiClient : BtcePublicApiClient
    {
        private readonly FakeHttpMessageHandler _handler;

        public TestableBtcePublicApiClient(string apiHostWithProtocol = null) : base(apiHostWithProtocol)
        {
            _handler = new FakeHttpMessageHandler();
        }

        public HttpRequestMessage LatestRequest => _handler.LatestRequest;

        public void MockHttpResponse(HttpResponseMessage httpResponseMessage)
        {
            _handler.SetHttpResponse(httpResponseMessage);
        }

        public void MockHttpResponse(string content, HttpStatusCode httpStatusCode)
        {
            var response = new HttpResponseMessage
                           {
                               StatusCode = httpStatusCode,
                               Content = new StringContent(content)
                           };

            MockHttpResponse(response);
        }

        protected override HttpClient CreateHttpClient()
        {
            return new HttpClient(_handler);
        }
    }
}
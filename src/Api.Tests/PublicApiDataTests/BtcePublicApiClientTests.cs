﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Api.Tests.PublicApiDataTests
{
    [TestFixture]
    public class BtcePublicApiClientTests
    {
        private void AssertApiMethodParams(HttpRequestMessage request, string apiMethodWithParams)
        {
            Assert.That(request.RequestUri.PathAndQuery, Is.EqualTo("/api/3" + apiMethodWithParams));
        }

        private static TestableBtcePublicApiClient CreatePublicClient(string apiHostAddress = null)
        {
            var publicClient = new TestableBtcePublicApiClient(apiHostAddress);
            publicClient.MockHttpResponse("{}", HttpStatusCode.OK);
            return publicClient;
        }

        [Test]
        public void Ctor_NoParams_WillUseDefaultApiHostAddress()
        {
            var publicApiClient = new BtcePublicApiClient();
            Assert.AreEqual("https://btc-e.com", publicApiClient.ApiHostAddress);
        }

        [Test]
        public void Ctor_PassApiHostUrl_ChangesApiHostAddress()
        {
            var publicApiClient = new BtcePublicApiClient("http://example.com");
            Assert.AreEqual(@"http://example.com", publicApiClient.ApiHostAddress);
        }

        [Test]
        public async Task GetInfoAsync_Default_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetApiInfoAsync();
            AssertApiMethodParams(client.LatestRequest, "/info");
        }

        [Test]
        public async Task GetLatestTradesAsync_MultiplePairPassed_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")});
            AssertApiMethodParams(client.LatestRequest, "/trades/ltc_rur-btc_usd?limit=150");
        }

        [Test]
        public async Task GetLatestTradesAsync_MultiplePairPassedWithValidLimit_ValidUrlCalled()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")}, 400);
            AssertApiMethodParams(client.LatestRequest, "/trades/ltc_rur-btc_usd?limit=400");
        }

        [Test]
        public async Task GetLatestTradesAsync_MultiplePairPassedZeroLimit_SetLimit150()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")}, 0);
            AssertApiMethodParams(client.LatestRequest, "/trades/ltc_rur-btc_usd?limit=150");
        }

        [Test]
        public async Task GetLatestTradesAsync_PassedPairWithInvalidLimit_SetLimit2000()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new BtcePair("btc_eur"), 3000);
            AssertApiMethodParams(client.LatestRequest, "/trades/btc_eur?limit=2000");
        }

        [Test]
        public async Task GetLatestTradesAsync_PassedPairWithValidLimit_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new BtcePair("btc_eur"), 200);
            AssertApiMethodParams(client.LatestRequest, "/trades/btc_eur?limit=200");
        }

        // TRADES

        [Test]
        public async Task GetLatestTradesAsync_WithPairDefaultLimit_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new BtcePair("ltc_rur"));
            AssertApiMethodParams(client.LatestRequest, "/trades/ltc_rur?limit=150");
        }

        // MARKET DEPTH
        [Test]
        public void GetMarketDepthAsync_BtcePairPassedAsNull_Throws()
        {
            var client = CreatePublicClient();
            Assert.That(async () => await client.GetMarketDepthAsync((BtcePair)null), Throws.ArgumentException);
        }

        [Test]
        public void GetMarketDepthAsync_BtcePairsPassedAsNull_Throws()
        {
            var client = CreatePublicClient();
            Assert.That(async () => await client.GetMarketDepthAsync((BtcePair[])null), Throws.ArgumentNullException);
        }


        [Test]
        public void GetMarketDepthAsync_ErrorJsonResponse_ThrowsBtceException()
        {
            var client = CreatePublicClient();
            client.MockHttpResponse("{success: 0, error: 'Invalid pair name: bbb_ccc'}", HttpStatusCode.OK);
            Assert.That(async () => await client.GetMarketDepthAsync(new BtcePair("bbb_ccc")),
                Throws.InstanceOf(typeof(BtceException)).With.Message.Contains("Invalid pair name: bbb_ccc"));
        }

        [Test]
        public async Task GetMarketDepthAsync_IfNoLimitPassed_UsesDefault150()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new BtcePair("ltc_rur"));
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur?limit=150");
        }

        [Test]
        public async Task GetMarketDepthAsync_LimitEqualsZero_SetLimit150()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new BtcePair("ltc_rur"), 0);
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur?limit=150");
        }


        [Test]
        public async Task GetMarketDepthAsync_MultiplePairPassedNoLimit_UseDefaultLimit()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")});
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur-btc_usd?limit=150");
        }

        [Test]
        public async Task GetMarketDepthAsync_MultiplePairPassedWithValidLimit_ValidUrlCalled()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")}, 400);
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur-btc_usd?limit=400");
        }

        [Test]
        public async Task GetMarketDepthAsync_MultiplePairWithLimitZero_SettLimit150()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")}, 0);
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur-btc_usd?limit=150");
        }

        [Test]
        public async Task GetMarketDepthAsync_PassedPairWithIvalidLimit_SetLimit2000()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new BtcePair("ltc_rur"), 5000);
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur?limit=2000");
        }

        [Test]
        public async Task GetMarketDepthAsync_PassedPairWithValidLimit_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new BtcePair("btc_eur"), 200);
            AssertApiMethodParams(client.LatestRequest, "/depth/btc_eur?limit=200");
        }

        [Test]
        public async Task GetMarketDepthAsync_WhenLimitGreater2000_SetLimit2000()
        {
            var client = CreatePublicClient();
            await client.GetMarketDepthAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")}, 2100);
            AssertApiMethodParams(client.LatestRequest, "/depth/ltc_rur-btc_usd?limit=2000");
        }

        [Test]
        public void GetTickerAsync_ErrorJsonResponse_ThrowsBtceException()
        {
            var client = CreatePublicClient();
            client.MockHttpResponse("{success: 0, error: 'Invalid pair name: bbb_ccc'}", HttpStatusCode.OK);
            Assert.That(async () => await client.GetTickerAsync(new BtcePair("bbb_ccc")),
                Throws.InstanceOf(typeof(BtceException)).With.Message.Contains("Invalid pair name: bbb_ccc"));
        }

        [Test]
        public async Task GetTickerAsync_PassedNull_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetTickerAsync(null);
            AssertApiMethodParams(client.LatestRequest, "/ticker");
        }

        [Test]
        public async Task GetTickerAsync_PassedNullItemAsBtcePair_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetTickerAsync(new BtcePair[] {null});
            AssertApiMethodParams(client.LatestRequest, "/ticker");
        }

        // TICKER
        [Test]
        public async Task GetTickerAsync_WithEmptyParams_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetTickerAsync();
            AssertApiMethodParams(client.LatestRequest, "/ticker");
        }

        [Test]
        public async Task GetTickerAsync_WithMulitplePairs_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetTickerAsync(new BtcePair("btc_eur"), new BtcePair("btc_usd"), new BtcePair("ltc_rur"));
            AssertApiMethodParams(client.LatestRequest, "/ticker/btc_eur-btc_usd-ltc_rur");
        }

        [Test]
        public async Task GetTickerAsync_WithOnePair_ShouldCallValidUrl()
        {
            var client = CreatePublicClient();
            await client.GetTickerAsync(new BtcePair("btc_eur"));
            AssertApiMethodParams(client.LatestRequest, "/ticker/btc_eur");
        }

        [Test]
        public void GetTradesAsync_ErrorJsonResponse_ThrowsBtceException()
        {
            var client = CreatePublicClient();
            client.MockHttpResponse("{success: 0, error: 'Invalid pair name: bbb_ccc'}", HttpStatusCode.OK);
            Assert.That(async () => await client.GetLatestTradesAsync(new BtcePair("bbb_ccc")),
                Throws.InstanceOf(typeof(BtceException)).With.Message.Contains("Invalid pair name: bbb_ccc"));
        }

        [Test]
        public async Task GetTradesAsync_LimitEqualsZero_SetLimit150()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new BtcePair("ltc_rur"), 0);
            AssertApiMethodParams(client.LatestRequest, "/trades/ltc_rur?limit=150");
        }

        [Test]
        public async Task GetTradesAsync_WhenLimitGreater2000_SetLimit2000()
        {
            var client = CreatePublicClient();
            await client.GetLatestTradesAsync(new[] {new BtcePair("ltc_rur"), new BtcePair("btc_usd")}, 2100);
            AssertApiMethodParams(client.LatestRequest, "/trades/ltc_rur-btc_usd?limit=2000");
        }
    }
}
﻿using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public sealed class CancelOrderResponse : FundsResponseBase
    {
        private CancelOrderResponse(JObject jObject) : base(jObject)
        {
        }

        public long OrderId { get; private set; }

        public static CancelOrderResponse FromJsonObject(JObject jsonObject)
        {
            var cancelOrderResponse = new CancelOrderResponse(jsonObject)
                                      {
                                          OrderId = jsonObject.Value<long>("order_id")
                                      };
            return cancelOrderResponse;
        }
    }
}
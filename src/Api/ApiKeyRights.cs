﻿using System;

namespace Api
{
    [Flags]
    public enum ApiKeyRights
    {
        None = 0,
        Info = 1 << 0,
        Trade = 1 << 1,
        Withdraw = 1 << 2
    }
}
﻿using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public sealed class TradeResponse : FundsResponseBase
    {
        private TradeResponse(JObject jObject) : base(jObject)
        {
        }

        public decimal Received { get; private set; }
        public decimal Remains { get; private set; }
        public long OrderId { get; private set; }

        internal static TradeResponse FromJsonObject(JObject jsonObject)
        {
            var tradeResponse = new TradeResponse(jsonObject)
                                {
                                    OrderId = jsonObject.Value<long>("order_id"),
                                    Received = jsonObject.Value<decimal>("received"),
                                    Remains = jsonObject.Value<decimal>("remains")
                                };
            return tradeResponse;
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Api.TradeApiData
{
    public sealed class ActiveOrderCollection : ReadOnlyCollection<OrderInfo>
    {
        internal ActiveOrderCollection(IList<OrderInfo> list) : base(list)
        {
        }

        internal static ActiveOrderCollection FromJsonObject(JObject jsonObject)
        {
            var activeOrders = new List<OrderInfo>();
            if (jsonObject != null)
            {
                foreach (var order in jsonObject.Children())
                {
                    var activeOrder = OrderInfo.FromJsonObject(new JObject(order));
                    activeOrders.Add(activeOrder);
                }
            }
            return new ActiveOrderCollection(activeOrders);
        }
    }
}
﻿using System;
using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class ApiKeyTests
    {
        [Test]
        public void Compare_TwoDifferentApiKeys_ReturnsFalse()
        {
            Assert.That(new ApiKey("apikey1"), Is.Not.EqualTo(new ApiKey("apikey2")));
        }

        [Test]
        public void Compare_TwoIdenticalApiKeys_ReturnsTrue()
        {
            Assert.That(new ApiKey("apikey"), Is.EqualTo(new ApiKey("apikey")));
        }

        [Test]
        public void Ctor_GivenNullValue_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new ApiKey(null));
        }
    }
}
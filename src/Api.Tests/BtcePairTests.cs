﻿using System;
using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class BtcePairTests
    {
        [TestCase("btc_usd", "btc", "usd")]
        [TestCase("ltc_usd", "ltc", "usd")]
        [TestCase("ltc_btc", "ltc", "btc")]
        [TestCase("ppc_btc", "ppc", "btc")]
        [TestCase("usd_rur", "usd", "rur")]
        public void Ctor_WhenPairNameValid_Success(string pairName, string firstPart, string secondPart)
        {
            var pair = new BtcePair(pairName);
            StringAssert.AreEqualIgnoringCase(firstPart, pair.Base);
            StringAssert.AreEqualIgnoringCase(secondPart, pair.Quote);
        }

        [TestCase("")]
        [TestCase("f")]
        [TestCase("111_222")]
        [TestCase("aaa_bb")]
        [TestCase("_")]
        [TestCase("_ddd")]
        [TestCase("ddd_")]
        [TestCase("ddd_dddd")]
        public void Ctor_InvalidPairName_Throws(string pairName)
        {
            Assert.Throws<FormatException>(() => new BtcePair(pairName));
        }


        [TestCase("btc_usd", "btc_usd")]
        [TestCase("btc_usd", "BTC_USD")]
        [TestCase("BTC_USD", "btc_usd")]
        public void EqualityTests(string pairName1, string pairName2)
        {
            Assert.That((BtcePair)pairName1 == (BtcePair)pairName2);
            Assert.AreEqual((BtcePair)pairName1, (BtcePair)pairName2);
        }

        [Test]
        public void Ctor_PairNameIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new BtcePair(null));
        }
    }
}
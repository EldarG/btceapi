﻿using System;

namespace Api
{
    public struct DateTimeRangeFilter
    {
        public DateTimeRangeFilter(DateTime startDateTime, DateTime endDateTime, uint numberOfTradesToReturn)
        {
            StartDateTime = startDateTime;
            EndDateTime = endDateTime;
            NumberOfTradesToReturn = numberOfTradesToReturn;
        }

        public DateTime StartDateTime { get; }
        public DateTime EndDateTime { get; }
        public uint NumberOfTradesToReturn { get; }
    }
}
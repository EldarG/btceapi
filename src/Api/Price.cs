﻿using System;
using System.Globalization;

namespace Api
{
    public struct Price : IEquatable<Price>
    {
        private decimal Value { get; }

        public Price(decimal value)
        {
            if (value <= 0)
                throw new ArgumentException("Value must be positive.", nameof(value));
            Value = value;
        }

        public bool Equals(Price other)
        {
            return Value == other.Value;
        }

        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Price && Equals((Price)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static bool operator ==(Price left, Price right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Price left, Price right)
        {
            return !left.Equals(right);
        }

        public static bool operator >(Price left, Price right)
        {
            return left.Value > right.Value;
        }

        public static bool operator <(Price left, Price right)
        {
            return left.Value < right.Value;
        }

        public static bool operator >=(Price left, Price right)
        {
            return left.Value >= right.Value;
        }

        public static bool operator <=(Price left, Price right)
        {
            return left.Value <= right.Value;
        }
    }
}
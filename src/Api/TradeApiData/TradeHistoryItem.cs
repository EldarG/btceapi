using System;

namespace Api.TradeApiData
{
    public sealed class TradeHistoryItem : IEquatable<TradeHistoryItem>
    {
        public TradeHistoryItem(long tradeId, BtcePair btcePair, TradeType tradeType, decimal amount, decimal price,
            long orderId, bool isYourOrder, DateTime tradeDateTime)
        {
            TradeId = tradeId;
            BtcePair = btcePair;
            TradeType = tradeType;
            Amount = amount;
            Price = price;
            OrderId = orderId;
            IsYourOrder = isYourOrder;
            TradeDateTime = tradeDateTime;
        }

        public long TradeId { get; }
        public BtcePair BtcePair { get; }
        public TradeType TradeType { get; }
        public decimal Amount { get; }
        public decimal Price { get; }
        public long OrderId { get; }
        public bool IsYourOrder { get; }
        public DateTime TradeDateTime { get; }


        public bool Equals(TradeHistoryItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return TradeId == other.TradeId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is TradeHistoryItem && Equals((TradeHistoryItem)obj);
        }

        public override int GetHashCode()
        {
            return TradeId.GetHashCode();
        }

        public static bool operator ==(TradeHistoryItem left, TradeHistoryItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TradeHistoryItem left, TradeHistoryItem right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return BtcePair.FormatOrder(TradeType, Amount, Price) + $" {TradeDateTime} oid: {OrderId}, tid {TradeId}";
        }
    }
}
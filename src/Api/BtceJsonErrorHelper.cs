﻿using System.Linq;
using Newtonsoft.Json.Linq;

namespace Api
{
    internal static class BtceJsonErrorHelper
    {
        private static readonly string[] _nonExceptionalErrorMessages = {"no trades", "no orders"};

        /// <summary>
        ///     Throws BtceException in case of error in json response
        /// </summary>
        /// <param name="jsonObject"></param>
        internal static void ThrowIfBtceError(this JObject jsonObject)
        {
            JToken successToken;
            if (jsonObject.TryGetValue("success", out successToken))
            {
                if (successToken.Value<int>() == 0)
                {
                    var errorMessage = "Unknown error.";
                    JToken errorToken;
                    if (jsonObject.TryGetValue("error", out errorToken))
                    {
                        errorMessage = errorToken.Value<string>();
                        if (_nonExceptionalErrorMessages.Contains(errorMessage))
                        {
                            // do nothing
                            return;
                        }
                    }

                    throw new BtceException(errorMessage);
                }
            }
        }
    }
}
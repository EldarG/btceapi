﻿using System.Collections.Generic;
using Api.PublicApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.PublicApiDataTests
{
    [TestFixture]
    public class TickerJsonMapTests
    {
        private TickerDictionary ExpectedTickers()
        {
            var dict = new Dictionary<BtcePair, Ticker>();
            var tickerBtc =
                new Ticker(109.88M, 91.14M, 100.51M, 1632898.2249M, 16541.51969M, 101.773M, 101.9M, 101.773M,
                    UnixTime.ToUtcDateTime(1370816308).ToLocalTime());
            var tickerLtc = new Ticker(50.99M, 24.9M, 33.33M, 1063299.99M, 106541.51969M, 10.8M, 10.9M, 10.1773M,
                UnixTime.ToUtcDateTime(1234567890).ToLocalTime());

            dict.Add(new BtcePair("btc_usd"), tickerBtc);
            dict.Add(new BtcePair("ltc_usd"), tickerLtc);

            return new TickerDictionary(dict);
        }


        private static string Json()
        {
            var json = @"{
                'btc_usd':{
                    'high':109.88,
                    'low':91.14,
                    'avg':100.51,
                    'vol':1632898.2249,
                    'vol_cur':16541.51969,
                    'last':101.773,
                    'buy':101.9,
                    'sell':101.773,
                    'updated':1370816308

                },
                'ltc_usd':{

                    'high':50.99,
                    'low':24.9,
                    'avg':33.33,
                    'vol':1063299.99,
                    'vol_cur':106541.51969,
                    'last':10.8,
                    'buy':10.9,
                    'sell':10.1773,
                    'updated':1234567890

                }
            }";

            return json;
        }

        [Test]
        public void TickerMapsToObjectCorrectly()
        {
            var actual = TickerDictionary.FromJsonObject(JObject.Parse(Json()));
            var expected = ExpectedTickers();

            var btcUsd = new BtcePair("btc_usd");
            var lct_usd = new BtcePair("ltc_usd");
            Assert.That(expected[btcUsd], Is.EqualTo(actual[btcUsd]));
            Assert.That(expected[lct_usd], Is.EqualTo(actual[lct_usd]));
        }
    }
}
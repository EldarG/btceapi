﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Api.Tests
{
    [TestFixture]
    public class BtceTradeApiClientTests
    {
        private static TestableBtceTradeApiClient CreateTradeApiClient(ApiKey apiKey = null, SecretKey secretKey = null,
            string apiHost = null)
        {
            apiKey = apiKey ?? new ApiKey("someApiKey");
            secretKey = secretKey ?? new SecretKey("someSecretKey");
            var tradeApiClient = new TestableBtceTradeApiClient(apiKey, secretKey, apiHost);
            tradeApiClient.NonceFactory = () => 1000;
            return tradeApiClient;
        }

        private void AssertSignKeyNonceApiMethodName(TestableBtceTradeApiClient tradeApiClient, string apiMethod)
        {
            var request = tradeApiClient.LatestRequest;
            var postData = tradeApiClient.LatestPostData;
            Assert.That(request.Method, Is.EqualTo(HttpMethod.Post));
            Assert.That(request.Headers.Contains("Sign"), "Header Sign missing");
            Assert.That(request.Headers.Contains("Key"), "Header Key missing");
            Assert.AreEqual("1000", postData["nonce"]);
            StringAssert.AreEqualIgnoringCase(apiMethod, postData["method"], "Invalid api method name");
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(long.MinValue)]
        public void GetOrderInfoAsync_GivenInvalidOrderId_Throws(long orderId)
        {
            var tradeApiClient = CreateTradeApiClient();
            Assert.That(async () => await tradeApiClient.GetOrderInfoAsync(orderId),
                Throws.ArgumentException.With.Message.Contains("positive"));
        }

        [TestCase(1)]
        [TestCase(10)]
        [TestCase(1000)]
        [TestCase(long.MaxValue)]
        public async Task GetOrderInfoAsync_GivenValidOrderId_CreatesCorrectPostData(long validOrderId)
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.GetOrderInfoAsync(validOrderId);
            var postData = tradeApiClient.LatestPostData;
            AssertSignKeyNonceApiMethodName(tradeApiClient, "OrderInfo");
            Assert.AreEqual(validOrderId.ToString(), postData["order_id"]);
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(long.MinValue)]
        public void CancelOrderAsync_GivenInvalidOrderId_Throws(long invalidOrderId)
        {
            var tradeApiClient = CreateTradeApiClient();
            Assert.That(async () => await tradeApiClient.CancelOrderAsync(invalidOrderId),
                Throws.ArgumentException.With.Message.Contains("positive"));
        }

        [TestCase(1)]
        [TestCase(10)]
        [TestCase(1000)]
        [TestCase(long.MaxValue)]
        public async Task CancelOrderAsync_GivenValidOrderId_CreatesCorrectPostData(long validOrderId)
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.CancelOrderAsync(validOrderId);
            var postData = tradeApiClient.LatestPostData;
            AssertSignKeyNonceApiMethodName(tradeApiClient, "CancelOrder");
            Assert.AreEqual(validOrderId.ToString(), postData["order_id"]);
        }

        [TestCase(10U, 200U, SortOrder.Asc)]
        [TestCase(77U, 1200U, SortOrder.Asc)]
        [TestCase(100U, 2000U, SortOrder.Asc)]
        [TestCase(uint.MinValue, uint.MaxValue, SortOrder.Desc)]
        public async Task GetTradeHistoryAsyncSkipTake_WithNotNullFilter_CreatesCorrectHttpRequest(uint skip, uint take,
            SortOrder sortOrder)
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.GetTradeHistoryAsync(new SkipTakeFilter(skip, take, sortOrder));

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            StringAssert.AreEqualIgnoringCase(skip.ToString(), postData["from"]);
            StringAssert.AreEqualIgnoringCase(take.ToString(), postData["count"]);
            StringAssert.AreEqualIgnoringCase(sortOrder.ToString(), postData["order"]);
        }

        [TestCase(10UL, 200UL, 100U, SortOrder.Asc)]
        [TestCase(100UL, 1200UL, 100U, SortOrder.Desc)]
        [TestCase(0UL, 999900UL, 50000U, SortOrder.Asc)]
        [TestCase(ulong.MinValue, ulong.MaxValue, 4000U, SortOrder.Desc)]
        public async Task GetTradeHistoryAsyncTradeIdRange_GivenNonDefaultTradeIdRangeFilter_CreatesCorrectHttpRequest(
            ulong startFromTradeId, ulong endOnTradeId, uint numberOfTradesToReturn, SortOrder sortOrder)
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await
                tradeApiClient.GetTradeHistoryAsync(new TradeIdRangeFilter(startFromTradeId, endOnTradeId,
                    numberOfTradesToReturn, sortOrder));

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            StringAssert.AreEqualIgnoringCase(startFromTradeId.ToString(), postData["from_id"]);
            StringAssert.AreEqualIgnoringCase(endOnTradeId.ToString(), postData["end_id"]);
            StringAssert.AreEqualIgnoringCase(numberOfTradesToReturn.ToString(), postData["count"]);
            StringAssert.AreEqualIgnoringCase(sortOrder.ToString(), postData["order"]);
        }

        [Test]
        public void GetActiveOrdersAsync_GivenInvalidBtcePair_Throws()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("some error");
            Assert.That(async () => await tradeApiClient.GetActiveOrdersAsync(new BtcePair("inv_ali")),
                Throws.InstanceOf(typeof(BtceException)).With.Message.Contains("some error"));
        }

        [Test]
        public void GetActiveOrdersAsync_GivenNull_Throws()
        {
            var tradeApiClient = CreateTradeApiClient();
            Assert.That(async () => await tradeApiClient.GetActiveOrdersAsync(null), Throws.ArgumentNullException);
        }

        [Test]
        public async Task GetActiveOrdersAsync_GivenValidBtcePair_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();

            await tradeApiClient.GetActiveOrdersAsync(new BtcePair("btc_usd"));
            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "ActiveOrders");
            Assert.AreEqual("btc_usd", postData["pair"]);
        }

        [Test]
        public async Task GetActiveOrdersAsync_NoArgs_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();

            await tradeApiClient.GetActiveOrdersAsync();

            AssertSignKeyNonceApiMethodName(tradeApiClient, "ActiveOrders");
        }

        [Test]
        public async Task GetActiveOrdersAsync_WhenNoOrders_ReturnsEmptyCollection()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("no orders");
            var orders = await tradeApiClient.GetActiveOrdersAsync();
            Assert.That(orders, Is.Empty);
        }

        [Test]
        public async Task GetStatsAsync_WhenCalled_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();

            await tradeApiClient.GetStatsAsync();

            AssertSignKeyNonceApiMethodName(tradeApiClient, "getInfo");
        }

        // TradeHistory filtering by DateTime range
        [Test]
        public async Task GetTradeHistoryAsyncDateTimeRange_GivenSpecificPair_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            var filter = new DateTimeRangeFilter(DateTime.Now.AddDays(-1), DateTime.Now, 1000);
            await tradeApiClient.GetTradeHistoryAsync(filter, new BtcePair("nmc_btc"));

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            StringAssert.AreEqualIgnoringCase(filter.StartDateTime.ToUnixTimestamp().ToString(), postData["since"]);
            StringAssert.AreEqualIgnoringCase(filter.EndDateTime.ToUnixTimestamp().ToString(), postData["end"]);
            StringAssert.AreEqualIgnoringCase(filter.NumberOfTradesToReturn.ToString(), postData["count"]);
            StringAssert.AreEqualIgnoringCase("ASC", postData["order"]);
            StringAssert.AreEqualIgnoringCase("nmc_btc", postData["pair"]);
        }

        [Test]
        public async Task GetTradeHistoryAsyncDateTimeRange_WhenNoTradesFound_ReturnsEmptyCollection()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("no trades");
            var trades =
                await tradeApiClient.GetTradeHistoryAsync(new DateTimeRangeFilter(DateTime.Today, DateTime.Now, 1000));
            Assert.That(trades, Is.Empty);
        }

        // TradeHistory filtering by SkipTake

        [Test]
        public void GetTradeHistoryAsyncSkipTake_GivenNullFilter_Throws()
        {
            var tradeApiClient = CreateTradeApiClient();
            Assert.That(async () => await tradeApiClient.GetTradeHistoryAsync((SkipTakeFilter)null),
                Throws.ArgumentNullException);
        }

        [Test]
        public async Task GetTradeHistoryAsyncSkipTake_GivenSpecificPair_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.GetTradeHistoryAsync(SkipTakeFilter.Default, new BtcePair("ltc_eur"));

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            Assert.AreEqual("0", postData["from"]);
            Assert.AreEqual("1000", postData["count"]);
            Assert.AreEqual("DESC", postData["order"]);
            Assert.AreEqual("ltc_eur", postData["pair"]);
        }

        [Test]
        public async Task GetTradeHistoryAsyncSkipTake_WhenNoTradeHistoryItemsFound_ReturnsEmptyCollection()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("no trades");
            var trades = await tradeApiClient.GetTradeHistoryAsync(SkipTakeFilter.Default);
            Assert.That(trades, Is.Empty);
        }

        [Test]
        public async Task GetTradeHistoryAsyncSkipTake_WithDefaultArgs_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.GetTradeHistoryAsync(SkipTakeFilter.Default);

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            Assert.AreEqual("0", postData["from"]);
            Assert.AreEqual("1000", postData["count"]);
            Assert.AreEqual("DESC", postData["order"]);
        }

        // TradeHistory filtering by TradeId range

        [Test]
        public async Task GetTradeHistoryAsyncTradeIdRange_GivenDefaultTradeIdRangeFilter_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.GetTradeHistoryAsync(TradeIdRangeFilter.Default);

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            Assert.AreEqual("0", postData["from_id"]);
            Assert.AreEqual("0", postData["end_id"]);
            Assert.AreEqual("1000", postData["count"]);
            Assert.AreEqual("DESC", postData["order"]);
        }

        [Test]
        public void GetTradeHistoryAsyncTradeIdRange_GivenNullFilter_Throws()
        {
            var tradeApiClient = CreateTradeApiClient();
            Assert.That(async () => await tradeApiClient.GetTradeHistoryAsync((TradeIdRangeFilter)null),
                Throws.ArgumentNullException);
        }

        [Test]
        public async Task GetTradeHistoryAsyncTradeIdRange_GivenSpecificPair_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            var filter = TradeIdRangeFilter.Default;
            await tradeApiClient.GetTradeHistoryAsync(filter, new BtcePair("nmc_btc"));

            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "TradeHistory");
            StringAssert.AreEqualIgnoringCase(filter.StartFromTradeId.ToString(), postData["from_id"]);
            StringAssert.AreEqualIgnoringCase(filter.EndOnTradeId.ToString(), postData["end_id"]);
            StringAssert.AreEqualIgnoringCase(filter.NumberOfTradesToReturn.ToString(), postData["count"]);
            StringAssert.AreEqualIgnoringCase(filter.SortOrder.ToString(), postData["order"]);
            StringAssert.AreEqualIgnoringCase("nmc_btc", postData["pair"]);
        }

        [Test]
        public async Task GetTradeHistoryAsyncTradeIdRange_GivenSpecificPairWhenNoTradeFound_ReturnsEmptyCollection()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("no trades");
            var trades = await tradeApiClient.GetTradeHistoryAsync(TradeIdRangeFilter.Default, new BtcePair("btc_rur"));
            Assert.That(trades, Is.Empty);
        }

        [Test]
        public async Task GetTradeHistoryAsyncTradeIdRange_WhenNoTradesFound_ReturnsEmptyCollection()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("no trades");
            var trades = await tradeApiClient.GetTradeHistoryAsync(TradeIdRangeFilter.Default);
            Assert.That(trades, Is.Empty);
        }

        [Test]
        public void TradeAsync_GivenInvalidBtcePair_Throws()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockBadResponse("invalid pair");
            Assert.That(
                async () =>
                    await tradeApiClient.TradeAsync(new BtcePair("inv_ali"), TradeType.Buy, new Price(1), new Amount(1)),
                Throws.InstanceOf(typeof(BtceException)).With.Message.Contains("invalid pair"));
        }

        [Test]
        public void TradeAsync_GivenNullForBtcePair_Throws()
        {
            var tradeApiClient = CreateTradeApiClient();
            Assert.That(async () => await tradeApiClient.TradeAsync(null, TradeType.Buy, new Price(1), new Amount(1)),
                Throws.ArgumentNullException);
        }

        [Test]
        public async Task TradeAsync_GivenValidArguments_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await tradeApiClient.TradeAsync(new BtcePair("btc_usd"), TradeType.Buy, new Price(1), new Amount(1));
            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "Trade");
            Assert.AreEqual("btc_usd", postData["pair"]);
            Assert.AreEqual("1", postData["rate"]);
            Assert.AreEqual("1", postData["amount"]);
            Assert.AreEqual("buy", postData["type"]);
        }

        [Test]
        public async Task TradeAsync_GivenValidArguments2_CreatesCorrectHttpRequest()
        {
            var tradeApiClient = CreateTradeApiClient();
            tradeApiClient.MockSuccessResponse();
            await
                tradeApiClient.TradeAsync(new BtcePair("ltc_eur"), TradeType.Sell, new Price(999.999M),
                    new Amount(199.88888M));
            var postData = tradeApiClient.LatestPostData;

            AssertSignKeyNonceApiMethodName(tradeApiClient, "Trade");
            Assert.AreEqual("ltc_eur", postData["pair"]);
            Assert.AreEqual("999.999", postData["rate"]);
            Assert.AreEqual("199.88888", postData["amount"]);
            Assert.AreEqual("sell", postData["type"]);
        }
    }
}
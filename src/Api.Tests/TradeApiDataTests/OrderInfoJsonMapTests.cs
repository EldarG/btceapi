﻿using Api.TradeApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.TradeApiDataTests
{
    [TestFixture]
    public class OrderInfoJsonMapTests
    {
        private string Json()
        {
            return @"{
                        '343152':{
                            'pair':'btc_usd',
                            'type':'sell',
                            'start_amount':13.345,
                            'amount':12.345,
                            'rate':485,
                            'timestamp_created':1342448420,
                            'status':0        
                        }
                    }";
        }


        [Test]
        public void FromJsonObjectShouldMapCorrectly()
        {
            var actual = OrderInfo.FromJsonObject(JObject.Parse(Json()));
            var expected = new OrderInfo(
                343152,
                new BtcePair("btc_usd"),
                TradeType.Sell,
                13.345M,
                12.345M,
                485,
                OrderStatus.Active,
                UnixTime.ToUtcDateTime(1342448420).ToLocalTime());

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
﻿using System;

namespace Api
{
    public static class UnixTime
    {
        private static DateTime _unixEpoch;

        static UnixTime()
        {
            _unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        public static uint Now => ToUnixTimestamp(DateTime.UtcNow);

        public static DateTime ToUtcDateTime(uint unixTimestamp)
        {
            return _unixEpoch.AddSeconds(unixTimestamp);
        }

        public static DateTime ToLocalDateTime(uint unixTimestamp)
        {
            return ToUtcDateTime(unixTimestamp).ToLocalTime();
        }

        public static uint ToUnixTimestamp(this DateTime d)
        {
            return (uint)(d - _unixEpoch).TotalSeconds;
        }
    }
}
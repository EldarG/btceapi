﻿using System;

namespace Api.PublicApiData
{
    public sealed class Ticker : IEquatable<Ticker>
    {
        internal Ticker(decimal highPrice, decimal lowPrice, decimal avgPrice, decimal volume, decimal volumeCurrency,
            decimal lastPrice, decimal buyPrice, decimal sellPrice, DateTime updatedAt)
        {
            HighPrice = highPrice;
            LowPrice = lowPrice;
            AvgPrice = avgPrice;
            Volume = volume;
            VolumeCurrency = volumeCurrency;
            LastPrice = lastPrice;
            BuyPrice = buyPrice;
            SellPrice = sellPrice;
            UpdatedAt = updatedAt;
        }

        public decimal HighPrice { get; }
        public decimal LowPrice { get; }
        public decimal AvgPrice { get; }
        public decimal Volume { get; }
        public decimal VolumeCurrency { get; }
        public decimal LastPrice { get; }
        public decimal BuyPrice { get; }
        public decimal SellPrice { get; }
        public DateTime UpdatedAt { get; }

        public bool Equals(Ticker other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return HighPrice == other.HighPrice && LowPrice == other.LowPrice && AvgPrice == other.AvgPrice &&
                   Volume == other.Volume && VolumeCurrency == other.VolumeCurrency && LastPrice == other.LastPrice &&
                   BuyPrice == other.BuyPrice && SellPrice == other.SellPrice && UpdatedAt.Equals(other.UpdatedAt);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is Ticker && Equals((Ticker)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = HighPrice.GetHashCode();
                hashCode = (hashCode*397) ^ LowPrice.GetHashCode();
                hashCode = (hashCode*397) ^ AvgPrice.GetHashCode();
                hashCode = (hashCode*397) ^ Volume.GetHashCode();
                hashCode = (hashCode*397) ^ VolumeCurrency.GetHashCode();
                hashCode = (hashCode*397) ^ LastPrice.GetHashCode();
                hashCode = (hashCode*397) ^ BuyPrice.GetHashCode();
                hashCode = (hashCode*397) ^ SellPrice.GetHashCode();
                hashCode = (hashCode*397) ^ UpdatedAt.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Ticker left, Ticker right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Ticker left, Ticker right)
        {
            return !Equals(left, right);
        }
    }
}
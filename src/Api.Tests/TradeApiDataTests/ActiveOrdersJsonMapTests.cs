﻿using System.Collections.Generic;
using Api.TradeApiData;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Api.Tests.TradeApiDataTests
{
    [TestFixture]
    public class ActiveOrdersJsonMapTests
    {
        private static string Json()
        {
            return @"{
                        '343152':{
                            'pair':'btc_usd',
                            'type':'sell',
                            'amount':12.345,
                            'rate':485,
                            'timestamp_created':1342448420,
                            'status':0            
                        },
                        '343153':{
                            'pair':'btc_usd',
                            'type':'buy',
                            'amount':132.345,
                            'rate':4.89,
                            'timestamp_created':1342448569,
                            'status':0            
                        },
                        '343156':{
                            'pair':'ltc_usd',
                            'type':'buy',
                            'amount':1302.345,
                            'rate':4850,
                            'timestamp_created':1342448553,
                            'status':0            
                        },                             
                }";
        }

        private ActiveOrderCollection ExpectedActiveOrderCollection()
        {
            var activeOrders = new List<OrderInfo>
                               {
                                   new OrderInfo(343152, new BtcePair("btc_usd"), TradeType.Sell, 12.345M, 12.345M, 485,
                                       OrderStatus.Active, UnixTime.ToUtcDateTime(1342448420).ToLocalTime()),
                                   new OrderInfo(343153, new BtcePair("btc_usd"), TradeType.Buy, 132.345M, 132.345M,
                                       4.89M,
                                       OrderStatus.Active,
                                       UnixTime.ToUtcDateTime(1342448569).ToLocalTime()),
                                   new OrderInfo(343156, new BtcePair("ltc_usd"), TradeType.Buy, 1302.345M, 1302.345M,
                                       4850,
                                       OrderStatus.Active,
                                       UnixTime.ToUtcDateTime(1342448553).ToLocalTime())
                               };

            return new ActiveOrderCollection(activeOrders);
        }

        [Test]
        public void ActiveOrdersJsonMapsCorrectly()
        {
            var actual = ActiveOrderCollection.FromJsonObject(JObject.Parse(Json()));
            var expected = ExpectedActiveOrderCollection();
            Assert.That(actual, Has.Count.EqualTo(3));
            Assert.That(actual, Is.EquivalentTo(expected));
        }
    }
}